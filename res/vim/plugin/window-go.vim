

function! s:window_go(dir)
  let l:old_winnr = winnr()
  if a:dir == "left"
    wincmd h
  elseif a:dir == "down"
    wincmd j
  elseif a:dir == "up"
    wincmd k
  elseif a:dir == "right"
    wincmd l
  else
    return "DO NOTHING"
  endif

  if l:old_winnr == winnr()
    call system("~/.xmonad/bin/xc go " . a:dir)
  endif

endfunction

nnoremap <silent> <Plug>(window_go_left)  :<C-u>call <SID>window_go("left")<CR>
nnoremap <silent> <Plug>(window_go_down)  :<C-u>call <SID>window_go("down")<CR>
nnoremap <silent> <Plug>(window_go_up)    :<C-u>call <SID>window_go("up")<CR>
nnoremap <silent> <Plug>(window_go_right) :<C-u>call <SID>window_go("right")<CR>

nmap <A-h> <Plug>(window_go_left)
nmap <A-j> <Plug>(window_go_down)
nmap <A-k> <Plug>(window_go_up)
nmap <A-l> <Plug>(window_go_right)
