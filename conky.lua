function conky_pad(n, value)
  return string.format('%' .. n .. 'i', conky_parse(value))
end

function conky_zpad(n, value)
  return string.format('%.' .. n .. 'i', conky_parse(value))
end

function conky_fmt(fmt, value)
  return string.format(fmt, conky_parse(value))
end

function conky_size(fmt, value)
  local v = tonumber(conky_parse(value))
  if 1000 < v then
    v = v / 1000
    return string.format('%03.0f' .. 'M', v)
  else
    if v < 10 then
      return string.format('%3.1f' .. 'K', v)
    end
    return string.format('%03.0f' .. 'K', v)
  end
end

function conky_bpad(n, value)
  local ni = tonumber(n)
  local param = tonumber(conky_parse(value))
  if param == nil then
    param = 0
  end
  local result = string.format('%0' .. n .. 'i', param)

  if string.len(result) > ni then
    return string.rep('X', ni)
  else
    return result
  end
end

function conky_without_spaces(n)
  return string.gsub(n, '%d', '')
end

function conky_uptime()
  local file = io.open('/proc/uptime', 'r')
  u = file:read()
  file:close()
  local sec = tonumber(string.match(u, '%d+'))
  local min = sec / 60 % 60
  local hour = sec / 60 / 60 % 24
  local day = sec / 60 / 60 / 24
  if 1 <= day then
    return string.format('%1.0fd%02.fh%02.0fm', day, hour, min)
  else
    return string.format('%02.fh%02.0fm', hour, min)
  end
end
