
import Anekos.Config.Action (setCurrentDirectory')
import Anekos.Config.Config
import Anekos.Config.Keyboard (myKeys, myNav2d)
import Anekos.Config.Layout (myLayoutHook)
import Anekos.Config.LogHook (mkMyLogHook)
import Anekos.Config.ManageHook (myManageHook)
import Anekos.Config.Mouse (myMouseBindings, mouseStartup)
import Anekos.Config.Scratchpad (scratchpadHook, scratchpadKeys)
import Anekos.Config.ScriptServerCommand (myScriptServerCommands)
import Anekos.Lib.FlashText (handleTimerEvent)
import Anekos.Lib.ScriptServer (scriptSeverEventHook)
import Anekos.Lib.Workspace (workspaceKeys)

import Data.Default (def)
import Data.Monoid (mempty)
import GHC.IO.Handle (hDuplicateTo)
import System.FilePath.Posix (joinPath)
import System.IO (openFile, stdout, hPutStrLn, hFlush, stderr, IOMode(AppendMode))
import XMonad
import XMonad.Actions.DynamicWorkspaces (renameWorkspace, selectWorkspace, addHiddenWorkspace)
import XMonad.Hooks.EwmhDesktops (ewmh, ewmhFullscreen)
import XMonad.Hooks.ManageDocks (docks)
import XMonad.Hooks.Minimize (minimizeEventHook)
import XMonad.Hooks.SetWMName (setWMName)
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.NamedScratchpad (namedScratchpadManageHook)
import qualified XMonad.Core as XC
import qualified XMonad.StackSet as SS

main = do
    initLogFile
    myLogHook <- mkMyLogHook
    xmonad $ myNav2d . ewmhFullscreen . ewmh . docks $ def
      { borderWidth        = 0
      , normalBorderColor  = myBorderColor
      , focusedBorderColor = myFocusedBorderColor
      , focusFollowsMouse  = False
      , handleEventHook    = minimizeEventHook
                         <+> scriptSeverEventHook myScriptServerCommands
                         <+> handleTimerEvent
      , layoutHook         = myLayoutHook
      , logHook            = myLogHook
      , manageHook         = myManageHook scratchpadHook
      , modMask            = myModMask
      , startupHook        = do setWMName "LG3D"
                                mouseStartup
                                myStartup
                                addHiddenWorkspace "NSP"
                                setCurrentDirectory' "/tmp/xmosh"
      , terminal           = myTerminal
      , workspaces         = myWorkspaces
      , mouseBindings      = myMouseBindings }
      `additionalKeysP` (myKeys ++ scratchpadKeys ++ workspaceKeys)


initLogFile :: IO ()
initLogFile = do
    init "std" stdout
    init "err" stderr
  where
    logFilePath name = do
        return $ joinPath ["/tmp/", "xmonad-" ++ name ++ ".log"]
    init name to = do
        fp <- logFilePath name
        h <- openFile fp AppendMode
        hDuplicateTo h to
        hPutStrLn to ""
        hPutStrLn to "<<XMonad Started>>"
        hPutStrLn to ""
        hFlush to
