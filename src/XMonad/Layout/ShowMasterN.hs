{-# LANGUAGE CPP, FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable, PatternGuards #-}


-----------------------------------------------------------------------------
-- |
-- Module      :  XMonad.Layout.ShowMasterN
-- Copyright   :  anekos 2013
-- License     :  BSD3
--
-- Maintainer  :  anekos
-- Stability   :  unstable
-- Portability :  unportable
--
-----------------------------------------------------------------------------


module XMonad.Layout.ShowMasterN (
  -- * Usage
  -- $usage

  showMasterN
) where

import Anekos.Lib.FlashText (flashText, FTConfig(..))

import XMonad
import XMonad.Layout.LayoutModifier



-- $usage
--
-- > import XMonad.Layout.ShowMasterN
--
-- > layoutHook = ShowMasterN 1 $ Tall 1 (3/100) (1/2)
--

data ShowMasterN a = ShowMasterN FTConfig Int deriving (Show, Read)

showMasterN :: FTConfig -> Int -> l a -> ModifiedLayout ShowMasterN l a
showMasterN c i = ModifiedLayout $ ShowMasterN c i

instance LayoutModifier ShowMasterN a where
    handleMess (ShowMasterN ftc n) m
        | Just (IncMasterN d) <- fromMessage m = do
            let c = max (n + d) 0
            flashText ftc $ 'n' : show c
            return $ Just $ ShowMasterN ftc c
    handleMess _ _ = return Nothing

