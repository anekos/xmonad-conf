
{-# LANGUAGE NoMonomorphismRestriction, TypeSynonymInstances, MultiParamTypeClasses, DeriveDataTypeable #-}

-----------------------------------------------------------------------------
-- |
-- Module      :  XMonad.Layout.Sample
-- Copyright   :  anekos 2013
-- License     :  BSD3
--
-- Maintainer  :  anekos
-- Stability   :  unstable
-- Portability :  portable
--
-- Layout for sample (Screenshot..)
-----------------------------------------------------------------------------

module XMonad.Layout.Sample (
  -- * Usage
  -- $usage

  SampleLayout(..)
) where


import XMonad
import XMonad.Layout.LayoutModifier (ModifiedLayout(..))
import XMonad.Layout.MultiToggle (Transformer(..))
import XMonad.Layout.ResizeScreen (withNewRectangle)



data SampleLayout = SampleLayout deriving (Read, Show, Eq, Typeable)

instance Transformer SampleLayout Window where
    transform SampleLayout x k = k (withNewRectangle (Rectangle 0 0 640 480) x) (\(ModifiedLayout _ x') -> x')
