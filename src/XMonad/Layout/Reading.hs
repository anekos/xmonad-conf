
{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses #-}

-----------------------------------------------------------------------------
-- |
-- Module      :  XMonad.Layout.Reading
-- Copyright   :  anekos 2012
-- License     :  BSD3
--
-- Maintainer  :  anekos
-- Stability   :  unstable
-- Portability :  unportable
-----------------------------------------------------------------------------


module XMonad.Layout.Reading (
  -- * Usage
  -- $usage

  Reading(..)
) where


import XMonad
import XMonad.StackSet (integrate)


-- $usage
--
-- > import XMonad.Layout.Reading
--
-- > layoutHook = Reading ||| Tall 1 (3/100) (1/2) ||| Full ...
--


data Reading a = Reading deriving (Read, Show)

instance LayoutClass Reading Window where
    doLayout Reading r s = do let layout = readingLayout r $ integrate s
                              return (layout, Nothing)


readingLayout :: Rectangle -> [a] -> [(a, Rectangle)]

readingLayout (Rectangle x y w h) ws = zip ws rs
  where
    rs  = [ Rectangle (fi $ w `div` 4) y (w `div` 2) h
          , Rectangle x y (w `div` 4) h
          , Rectangle (fi $ w `div` 4 * 3) y (w `div` 4) h ]
    fi = fromIntegral
