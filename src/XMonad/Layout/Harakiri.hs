
{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses #-}

-----------------------------------------------------------------------------
-- |
-- Module      :  XMonad.Layout.Harakiri
-- Copyright   :  anekos 2012
-- License     :  BSD3
--
-- Maintainer  :  anekos
-- Stability   :  unstable
-- Portability :  unportable
--
-- Japanese tatami style layout (for Harakiri).
-- If you are samurai and you wish to do harakiri, You should use this layout.
-----------------------------------------------------------------------------


module XMonad.Layout.Harakiri (
  -- * Usage
  -- $usage

  Harakiri(..)
) where


import XMonad
import XMonad.StackSet (integrate)


-- $usage
--
-- > import XMonad.Layout.Harakiri
--
-- > layoutHook = Harakiri ||| Tall 1 (3/100) (1/2) ||| Full ...
--


data Harakiri a = Harakiri deriving (Read, Show)

instance LayoutClass Harakiri Window where
    doLayout Harakiri r s = do let layout = harakiriLayout r $ integrate s
                               return (layout, Nothing)


harakiriLayout :: Rectangle -> [a] -> [(a, Rectangle)]
harakiriLayout (Rectangle x y w h) ws = zip ws rs
  where
    rs  = [ Rectangle x y (t w) (o h)
          , Rectangle (x + t w) y (o w) (t h)
          , Rectangle (x + o w) (y + t h) (t w) (o h)
          , Rectangle x (y + o h) (o w) (t h)
          , Rectangle (x + o w) (y + o h) (o w) (o h) ]
    t x' = fromIntegral $ x' `div` 3 * 2
    o x' = fromIntegral $ x' `div` 3
