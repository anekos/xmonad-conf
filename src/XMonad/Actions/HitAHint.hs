----------------------------------------------------------------------------
-- |
-- Module      :  XMonad.Actions.HitAHint
-- Copyright   :  (c) anekos 2012
-- License     :  BSD3-style (see LICENSE)
--
-- Maintainer  :  anekos <anekos@snca.net>
-- Stability   :  unstable
-- Portability :  not portable
--
-- This provides hit-a-hints for window selection.
--
-- You can quickly select window by just typing a few characters.
-----------------------------------------------------------------------------


module XMonad.Actions.HitAHint (
    -- * Usage
    -- $usage

    showWindowHints,
    HAHConfig(..),
    simpleHints
) where


import Control.Applicative ((<$>))
import Control.Monad (filterM, forM_, when)
import Data.Default (Default, def)
import Data.List (isPrefixOf, sortBy)
import Data.Ord (comparing)
import Data.Ratio ((%))
import Foreign.C (CInt)
import XMonad
import XMonad.Hooks.FadeInactive (setOpacity)
import qualified XMonad.StackSet as SS
import XMonad.Util.Font (initXMF, textWidthXMF, textExtentsXMF, releaseXMF, Align(..))
import XMonad.Util.XUtils (fi, createNewWindow, showWindow, deleteWindow, paintAndWrite)

-- $usage
--
-- You can use this module with the following in your @~\/.xmonad\/xmonad.hs@:
--
-- >    import XMonad.Actions.HitAHint
--
-- Then add a keybinding, e.g.
--
-- >    , ((modm, xK_f), showWindowHints defaultHAHConfig focus
--
--

-- | Hint style and hint text generator.
data HAHConfig = HAHC
    { hahFont        :: String                             -- ^ Font name
    , hahColor       :: String                             -- ^ Text color
    , hahBgcolor     :: String                             -- ^ Background color
    , hahBorderColor :: String   -- ^ Border color
    , hahBorderWidth :: Int      -- ^ Border width
    , hahOpacity     :: Rational -- ^ Window opacity
    , hahHints       :: [Window] -> X [(Window, String)]   -- ^ Hint texts generator
    , hahPadding     :: Int                                -- ^ Padding for text and window
    }


-- | A basic configuration for showWindowHints.
instance Default HAHConfig where
  def = HAHC
    { hahFont         = "-misc-fixed-*-*-*-*-20-*-*-*-*-*-*-*"
    , hahColor        = "white"
    , hahBgcolor      = "#285577"
    , hahBorderColor  = "#aaaaaa"
    , hahBorderWidth  = 1
    , hahOpacity      = 9%10
    , hahHints        = simpleHints ['a' .. 'z']
    , hahPadding      = 5 }


-- | Show hint texts on each windows.
--
-- When user typed the hint text, run the given action to selected window.
showWindowHints :: HAHConfig -> (Window -> X ()) -> X ()
showWindowHints c f = do
    XConf { display = dpy, theRoot = rootw } <- ask
    ws  <- targetWindows dpy >>= hahHints c
    hints <- mapM (uncurry $ showHint c dpy) ws
    io $ sync dpy False
    status <- io $ grabKeyboard dpy rootw False grabModeAsync grabModeAsync currentTime
    when (status == grabSuccess) $ do
      matched <- eventLoop ws dpy
      io $ ungrabKeyboard dpy currentTime
      whenJust matched f
    forM_ hints deleteWindow


-- | A simple hint texts generator.
simpleHints :: String -> [Window] -> X [(Window, String)]
simpleHints cs ws = return $ zip ws hints
  where
    chars = map return cs
    hints = chars ++ [ a ++ b | a <- hints, b <- chars]


-- | Show hint window on given window.
showHint :: HAHConfig -> Display -> Window -> String -> X Window
showHint c d w s = do
    font <- initXMF $ hahFont c
    -- size
    WindowAttributes {
      wa_x = sx, wa_y = sy, wa_width = sw, wa_height = sh
    } <- io $ getWindowAttributes d w
    width <- (+ hahPadding c) <$> textWidthXMF d font s
    hight <- (+ hahPadding c) . fi <$> uncurry (+) <$> textExtentsXMF font s
    let y = fi sy + (fi sh - hight) `div` 2
        x = fi sx + (fi sw - width) `div` 2
    let r = Rectangle (fi x) (fi y) (fi width) (fi hight)
    -- window
    hw <- createNewWindow r Nothing "" True
    setWindowAttrs hw
    showWindow hw
    paintAndWrite hw font (fi width) (fi hight) 0 (hahBgcolor c) "" (hahColor c) (hahBgcolor c) [AlignCenter] [s]
    releaseXMF font
    return hw
  where
    setWindowAttrs w' = do
      io $ setWindowBorderWidth d w' $ fi $ hahBorderWidth c
      bc <- io $ initColor d $ hahBorderColor c
      whenJust bc $ io . setWindowBorder d w'
      setOpacity w' $ hahOpacity c


targetWindows :: Display -> X [Window]
targetWindows dpy = do
    cur <- gets (screenWindows . SS.current . windowset)
    vis <- gets (concatMap screenWindows . SS.visible . windowset)
    wrs <- filterM (isMapped dpy) (cur ++ vis) >>= mapM (withWindowRect dpy)
    return $ map fst $ sortByPos wrs


type Rect = (CInt, CInt, CInt, CInt)


inputMatch :: String -> [(Window, String)] -> (String -> X (Maybe Window)) -> X (Maybe Window)
inputMatch input names cand =
    case filter (isPrefixOf input . snd) names of
      []        -> return Nothing
      [(w, _)]  -> return $ Just w
      _         -> cand input


inputMatch' :: String -> [(Window, String)] -> X (Maybe Window)
inputMatch' input names =
    case filter ((== input) . snd) names of
      [(w, _)]  -> return $ Just w
      _         -> return Nothing


eventLoop :: [(Window, String)] -> Display -> X (Maybe Window)
eventLoop ws dpy = loop ""
  where
    loop input = do
      ec <- io $
        allocaXEvent $ \e -> do
          maskEvent dpy keyPressMask e
          KeyEvent {ev_keycode = keycode, ev_state = keymask} <- getEvent e
          keysym <- keycodeToKeysym dpy keycode 0
          (_, s) <- io $ lookupString $ asKeyEvent e
          return (keymask, keysym, s)
      case ec of
         (0, sym, _) | sym == xK_Escape -> return Nothing
                     | sym == xK_Return -> inputMatch' input ws
         (_, _, s)                      -> inputMatch (input ++ s) ws loop


-- | Return the windows on given screen.
screenWindows :: SS.Screen i l Window sid sd -> [Window]
screenWindows = SS.integrate' . SS.stack . SS.workspace


withWindowRect :: Display -> Window -> X (Window, Rect)
withWindowRect dpy w = do
    WindowAttributes {
      wa_x = cx, wa_y = cy, wa_width = cw, wa_height = ch
    } <- io $ getWindowAttributes dpy w
    return (w, (cx, cy, cw, ch))


-- | Left window ... Right wingow.
sortByPos :: [(Window, Rect)] -> [(Window, Rect)]
sortByPos = sortBy $ comparing $ \(_, (x, y, _, _)) -> [x, y]


isMapped :: Display -> Window -> X Bool
isMapped dpy w = do wa <- io $ getWindowAttributes dpy w
                    return (wa_map_state wa /= waIsUnmapped)
