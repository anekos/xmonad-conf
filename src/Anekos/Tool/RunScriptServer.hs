
import Graphics.X11.Xlib
import Graphics.X11.Xlib.Extras
import System.Environment


main :: IO ()
main = do
    args <- getArgs
    pn <- getProgName
    mapM_ sendCommand args


sendCommand :: String -> IO ()
sendCommand s = do
    dpy <- openDisplay ""
    rw  <- rootWindow dpy $ defaultScreen dpy
    a <- internAtom dpy "XMONAD_RUN_SCRIPT" False
    allocaXEvent $ \e -> do
      setEventType e clientMessage
      setClientMessageEvent e rw a 32 (fromIntegral (read s)) currentTime
      sendEvent dpy rw False structureNotifyMask e
      sync dpy False
