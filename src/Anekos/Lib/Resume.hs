
{-# LANGUAGE ScopedTypeVariables #-}

module Anekos.Lib.Resume (
    getXMonadStatus,
    reload
) where


import Anekos.Lib.Script (scriptPath)

import Control.Applicative
import Control.Exception.Extensible (catch, SomeException(..))
import Control.Monad (void, when, filterM)
import Data.List ((\\))
import Data.Map (toList)
import Data.Maybe (mapMaybe)
import System.Directory (removeFile, getTemporaryDirectory, getModificationTime, doesDirectoryExist, getDirectoryContents, getHomeDirectory)
import System.Environment (withArgs)
import System.Exit (ExitCode(..))
import System.FilePath ((</>), takeExtension)
import System.FilePath.Posix (joinPath)
import System.IO (hPutStrLn, stderr, IOMode(WriteMode), withFile)
import System.Posix.Process (executeFile, forkProcess)
import System.Process (runCommand, waitForProcess, runProcess)
import qualified XMonad.StackSet as SS
import XMonad



reload :: X ()
reload = do
    finalize
    spawn "~/.xmonad/reload"


getXMonadStatus :: X String
getXMonadStatus = gets $ show . (\s -> windowSetData s : extState s)
  where
    windowSetData   = show . SS.mapLayout show . windowset
    extState        = return . show . mapMaybe maybeShow . toList . extensibleState
    maybeShow (t, Right (PersistentExtension ext)) = Just (t, show ext)
    maybeShow (t, Left str)                        = Just (t, str)
    maybeShow _                                    = Nothing


--XXX Ignore waitForProcsss error.
finalize :: X ()
finalize = do fp <- scriptPath "finalize"
              void $ io (runCommand fp >>= waitForProcess)
                -- `catch` \(_ :: IOError) -> return ()
