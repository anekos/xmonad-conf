
{-# LANGUAGE NoMonomorphismRestriction, ExplicitForAll, FlexibleContexts #-}

module Anekos.Lib.XUtils (
    copyToClipboard,
    copyToClipboard',
    focusAnd,
    getAllScreens,
    getAllWindows,
    getCurrentLayout,
    getCurrentScreen,
    getCurrentWorkspaces,
    getFocus,
    getVisibleWindows,
    getWindowCwd,
    getWindowPid,
    isFloating,
    waitKeyRelease,
    withAllOfScreen,
    withFocusRestore
) where


import Anekos.Lib.IO (redirectTo)
import Anekos.Lib.Utils

import Control.Monad (when, unless, forM_)
import qualified Data.Map as M
import System.Posix (ProcessID)
import System.Posix.Files (readSymbolicLink)
import XMonad
import XMonad.StackSet as SS
import XMonad.Operations as O
import XMonad.Util.WindowProperties (getProp32s)


copyToClipboard :: String -> X ()
copyToClipboard x = io $ redirectTo x "xclip" ["-i"]


copyToClipboard' :: Show a => a -> X ()
copyToClipboard' x = io $ redirectTo (show x) "xclip" ["-i"]


getFocus :: X (Maybe Window)
getFocus = withWindowSet <<$ SS.peek


getWindowPid :: Window -> X (Maybe ProcessID)
getWindowPid w = do
    p <- getProp32s "_NET_WM_PID" w
    return $ case p of
        Just [x] -> Just (fromIntegral x)
        _        -> Nothing


getWindowCwd:: Window -> X (Maybe String)
getWindowCwd w = do
    pid <- getWindowPid w
    case pid of
         Just pid' -> Just <$> (io $ readSymbolicLink $ "/proc/" ++ show pid' ++ "/cwd")
         Nothing -> return Nothing


getCurrentScreen :: X ScreenId
getCurrentScreen = withWindowSet <<$ screen . current


getAllScreens :: MonadState XState m => m [SS.Screen WorkspaceId (Layout Window) Window ScreenId ScreenDetail]
getAllScreens = do
    ws <- gets windowset
    return $ current ws : visible ws


getVisibleWindows :: X [Window]
getVisibleWindows = withWindowSet <<$ concatMap (integrate' . stack . workspace) . screens


getCurrentWorkspaces :: X [WorkspaceId]
getCurrentWorkspaces = withWindowSet <<$ map (tag . workspace) . screens


getCurrentLayout :: X String
getCurrentLayout = withWindowSet <<$ description . SS.layout . SS.workspace . SS.current


getAllWindows :: X [Window]
getAllWindows = withWindowSet <<$ allWindows


focusAnd :: (Window -> X ()) -> Window -> X ()
focusAnd f w = O.focus w >> f w


isFloating :: Window -> X Bool
isFloating w = withWindowSet (return . M.member w . floating)


withAllOfScreen :: (Window -> X ()) -> X ()
withAllOfScreen f = do
    ws <- gets $ concatMap (integrate' . stack . workspace) . SS.screens . windowset
    forM_ ws f


withFocusRestore :: X () -> X ()
withFocusRestore = (>> withFocused O.focus)


waitKeyRelease :: [KeySym] -> X ()
waitKeyRelease ks = do
    XConf { display = dpy, theRoot = rootw } <- ask
    status <- io $ grabKeyboard dpy rootw False grabModeAsync grabModeAsync currentTime
    when (status == grabSuccess) $ do
      wkrEventLoop ks dpy rootw
      io $ ungrabKeyboard dpy currentTime


wkrEventLoop :: [KeySym] -> Display -> Window -> X ()
wkrEventLoop ks dpy _ = loop
  where
    loop = do
      keysym <- io $
        allocaXEvent $ \e -> do
          maskEvent dpy keyReleaseMask e
          KeyEvent {ev_keycode = keycode} <- getEvent e
          keycodeToKeysym dpy keycode 0
      -- io $ sync dpy True >> refresh
      unless (keysym `elem` ks) loop
