module Anekos.Lib.Query (
  (=$?),
  (=^?),
  className1,
  wmIconName,
  layoutName,
  wmRole,
  shellName,
  stringProperty',
  title,
  wmName,
  hasProp
) where


import Control.Exception.Extensible (bracket, catch, SomeException(..))
import Data.List (isPrefixOf, isInfixOf, isSuffixOf)
import Data.Maybe (fromMaybe, isJust)
import XMonad
import qualified XMonad.StackSet as SS



shellName :: String -> Query Bool
shellName x = title =? x <||> wmIconName =? x <||> className1 =? x <||> wmRole =? x


className1 :: Query String
className1 = ask >>= \w -> liftX $ do
    d <- asks display
    let getProp = (internAtom d "WM_CLASS" False >>= getTextProperty d w) `catch` \(SomeException _) -> getTextProperty d w wM_CLASS
        extract prop = do l <- wcTextPropertyToTextList d prop
                          return $ if null l then "" else head l
    io $ bracket getProp (xFree . tp_value) extract `catch` \(SomeException _) -> return ""


wmRole :: Query String
wmRole = stringProperty "WM_WINDOW_ROLE"


wmName :: Query String
wmName = stringProperty "WM_NAME"


wmIconName :: Query String
wmIconName = stringProperty "WM_ICON_NAME"


layoutName :: Query String
layoutName = liftX $ gets (description . SS.layout . SS.workspace . SS.current . windowset)


stringProperty' :: String -> Query String
stringProperty' p = ask >>= (\w -> liftX $ withDisplay $ \d -> fmap (fromMaybe "") $ getStringProperty d w p)


getStringProperty' :: Display -> Window -> String -> X (Maybe String)
getStringProperty' d w p = do
  a  <- getAtom p
  md <- io $ getWindowProperty8 d a w
  let str = map (toEnum . fromIntegral) . filter (0 <)
  return $ fmap str md


hasProp :: String -> Query Bool
hasProp p = do
  w <- ask
  liftX $ withDisplay $ \d -> fmap isJust $ getStringProperty d w p



-- Op

(=^?), (=*?), (=$?) :: Query String -> String -> Query Bool
q =^? x = fmap (isPrefixOf x) q
q =*? x = fmap (isInfixOf x) q
q =$? x = fmap (isSuffixOf x) q


