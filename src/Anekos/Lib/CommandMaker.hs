
module Anekos.Lib.CommandMaker (
    Cmd,
    TitledPrompt(..),
    doCommand,
    doCommand',
    mkCommandPrompt
) where


import Control.Monad (void)
import Data.List (find)
import Data.Maybe (isJust)
import XMonad
import XMonad.Prompt as P ( XPConfig, XPrompt(..), mkXPrompt
                          , getNextCompletion
                          , searchPredicate )


data TitledPrompt = TitledPrompt String

instance XPrompt TitledPrompt where
    showXPrompt (TitledPrompt t)  = t ++ ": "
    commandToComplete _ c   = c
    nextCompletion    _     = getNextCompletion


type Cmd = (String, X ())


doCommand :: [Cmd] -> String -> X ()
doCommand cs input = void $ doCommand' cs input


doCommand' :: [Cmd] -> String -> X Bool
doCommand' cs input = do let found = find ((== input) . fst) cs
                         whenJust found snd
                         return $ isJust found


mkCommandPrompt :: XPConfig -> String -> [Cmd] -> X ()
mkCommandPrompt xpc t cs = mkXPrompt (TitledPrompt t) xpc compl action
  where
    compl  s = return $ filter (searchPredicate xpc s) $ map fst cs
    action   = doCommand cs
