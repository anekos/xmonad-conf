
module Anekos.Lib.Script (
    getScriptOut1,
    runScript,
    scriptPath
) where


import Anekos.Lib.IO (commandResult1)

import System.FilePath.Posix (joinPath)
import XMonad
import XMonad.Util.Run (safeSpawn)


runScript :: String -> [String] -> X ()
runScript name args = scriptPath name >>= flip safeSpawn args


getScriptOut1 :: String -> [String] -> X String
getScriptOut1 name args = scriptPath name >>= io . flip commandResult1 args


scriptPath :: String -> X String
scriptPath name = do dir <- asks (cfgDir . directories)
                     return $ joinPath [dir, "script", name]
