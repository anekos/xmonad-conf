
{-# LANGUAGE DeriveDataTypeable #-}

module Anekos.Lib.SubKeyMode (
    KeyModeTable,
    enterKeyMode,
    withCount
) where


import XMonad
import XMonad.Util.ExtensibleState as XS
import Control.Monad (when, unless)
import qualified Data.Map as M
import Data.Maybe (isJust, fromMaybe)


type KeyModeKey = (KeyMask, KeySym)
type KeyModeTable = M.Map String (M.Map KeyModeKey (X ()))
type KeyModeName = String

data Count = Count (Maybe Int) deriving (Typeable, Show)

instance ExtensionClass Count where
    initialValue = Count Nothing


doKeyAction :: KeyModeTable -> KeyModeName -> KeyMask -> KeySym -> X Bool
doKeyAction table mode mask sym = do
    let act = M.lookup mode table >>= M.lookup (mask, sym)
    whenJust act (>> XS.put (Count Nothing))
    return $ isJust act


eventLoop :: KeyModeTable -> KeyModeName -> Display -> Window -> X ()
eventLoop table mode dpy _ = loop
  where
    loop = do
      (keymask, keysym) <- io $
        allocaXEvent $ \e -> do
          maskEvent dpy keyPressMask e
          KeyEvent {ev_keycode = keycode, ev_state = keymask} <- getEvent e
          keysym <- keycodeToKeysym dpy keycode 0
          return (keymask, keysym)

      if (keymask == 0) && (xK_0 <= keysym) && (keysym <= xK_9)
        then do
          Count now <- XS.get
          let now' = fromMaybe 0 now
          XS.put $ Count $ Just (now' * 10 + fromIntegral (toInteger (keysym - xK_0)))
          return ()
        else do
          done <- doKeyAction table mode keymask keysym
          when done $ XS.put $ Count Nothing
          io $ sync dpy True
          refresh
      unless (isEscapeKey keymask keysym) loop


enterKeyMode :: KeyModeTable -> KeyModeName -> X ()
enterKeyMode table mode = do
    XConf { display = dpy, theRoot = rootw } <- ask
    status <- io $ grabKeyboard dpy rootw False grabModeAsync grabModeAsync currentTime
    when (status == grabSuccess) $ do
      eventLoop table mode dpy rootw
      io $ ungrabKeyboard dpy currentTime


withCount :: Integral i => i -> (i -> X ()) -> X ()
withCount defVal f = do
    Count cnt <- XS.get
    f $ fromIntegral $ maybe defVal fromIntegral cnt


isEscapeKey :: KeyMask -> KeySym -> Bool
isEscapeKey 0 ks
    | ks == xK_Escape   = True
    | ks == xK_q        = True
isEscapeKey _ _         = False
