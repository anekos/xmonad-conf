{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable, TypeSynonymInstances, PatternGuards #-}

module Anekos.Lib.VerticalSubGaps (
    VerticalSubGaps,
    verticalSubGaps
) where

import XMonad
import Graphics.X11 (Rectangle(..))

import XMonad.Layout.LayoutModifier
import XMonad.Util.XUtils (fi)
import GHC.Word (Word32)


--                                       Bar Height
--                                       Main   Sub
data VerticalSubGaps a = VerticalSubGaps Word32 Word32 deriving (Read, Show)

instance LayoutModifier VerticalSubGaps Window where
    modifyLayout g w r = runLayout w (applyGaps g r)

applyGaps :: VerticalSubGaps a -> Rectangle -> Rectangle
applyGaps (VerticalSubGaps main sub) (Rectangle x y 1920 1080) = Rectangle x y 1920 (1080 - main)
applyGaps (VerticalSubGaps main sub) (Rectangle x y 1080 1920) = Rectangle x (fi sub) 1080 (1920 - sub)
applyGaps (VerticalSubGaps main sub) (Rectangle x y 3840 2160) = Rectangle x y 3840 (2160 - main)
applyGaps (VerticalSubGaps main sub) (Rectangle x y 2160 3840) = Rectangle x (fi sub) 2160 (3840 - sub - main)
applyGaps _ r = r


verticalSubGaps :: LayoutClass l a => Word32 -> Word32 -> l a -> ModifiedLayout VerticalSubGaps l a
verticalSubGaps main sub = ModifiedLayout $ VerticalSubGaps main sub
