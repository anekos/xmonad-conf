
{-# LANGUAGE DeriveDataTypeable #-}

module Anekos.Lib.Notify (
    changeNotifyMethod,
    notify
) where


import XMonad hiding (sendMessage)
import XMonad.Util.Run (safeSpawn)
import XMonad.Util.ExtensibleState as XS


data NotifyMethod = DZenNotify | GrowlNotify deriving (Typeable, Read, Show)

instance ExtensionClass NotifyMethod where
    initialValue = GrowlNotify
    extensionType = PersistentExtension


notify :: String -> X ()
notify msg = do
    method <- XS.get
    notifyFunc method msg


notifyFunc :: NotifyMethod -> String -> X ()
notifyFunc DZenNotify   = notifyDzen
notifyFunc GrowlNotify  = notifyGrowl


changeNotifyMethod :: X ()
changeNotifyMethod = do
    method <- XS.get
    XS.put $ next method
    notify "Notify method has been changed."
  where
    next DZenNotify   = GrowlNotify
    next GrowlNotify  = DZenNotify


notifyGrowl :: String -> X ()
notifyGrowl s =
    safeSpawn "notify-send" ["-u", "normal", "XMonad", s]


notifyDzen :: String -> X ()
notifyDzen = const $ return ()
    -- TODO
    -- dzenConfig $
    -- args [center 600 100 0, font (myFont 16), bg "lightblue", fg "black", timeout 1]

