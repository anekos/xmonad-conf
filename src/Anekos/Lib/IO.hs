
module Anekos.Lib.IO (
    commandResult1,
    commandResult,
    redirectTo
) where


import Prelude hiding (pi)
import Control.Applicative ((<$>))
import System.IO (hPutStr, hGetLine, hClose, hGetContents)
import System.Process (createProcess, proc, StdStream(..), CreateProcess(..))


commandResult1 :: String -> [String] -> IO String
commandResult1 cmd args = do
    process <- createProcess (proc cmd args) { std_out = CreatePipe }
    pi process
  where
    pi (_, Just hOut, _, _) = do
        l <- hGetLine hOut
        hClose hOut
        return l
    pi _                    = error "Unable to start process"


commandResult :: String -> [String] -> IO [String]
commandResult cmd args = do
    process <- createProcess (proc cmd args) { std_out = CreatePipe }
    pi process
  where
    pi (_, Just hOut, _, _) = lines <$> hGetContents hOut
    pi _                    = error "Unable to start process"


redirectTo :: String -> String -> [String] -> IO ()
redirectTo text cmd args = do
    process <- createProcess (proc cmd args) { std_in = CreatePipe }
    pi process
  where
    pi (Just hIn, _, _, _) = do
        l <- hPutStr hIn text
        hClose hIn
        return l
    pi _                   = error "Unable to start process"

