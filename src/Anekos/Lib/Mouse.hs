
module Anekos.Lib.Mouse (
    getMousePosition,
    mouseClick,
    mouseClickWithNoMod,
    moveMouseRel,
    moveMouseWindowCenter,
    moveMouseWindowCenter',
    moveMouseX,
    moveMouseX',
    moveMouseY,
    moveMouseY',
    updateMousePosition
) where


import Anekos.Lib.Window (currentScreenRect, windowRect)
import Anekos.Lib.Utils

import XMonad
import qualified XMonad.StackSet as SS
import XMonad.Util.Run (safeSpawn)
import XMonad.Util.XUtils (fi)
import Control.Monad (when)
import GHC.Word (Word32)


getMousePosition :: X D
getMousePosition = do
    XConf { display = dpy, theRoot = rootw } <- ask
    (_, _, _, x, y, _, _, _) <- io $ queryPointer dpy rootw
    return (fi x, fi y)


updateMousePosition :: D -> X ()
updateMousePosition (x, y) = do
    XConf { display = dpy, theRoot = rootw } <- ask
    focused <- withWindowSet <<$ SS.peek
    io $ warpPointer dpy none rootw 0 0 0 0 (fi x) (fi y)
    (_, _, _, w) <- io $ translateCoordinates dpy rootw rootw (fi x) (fi y)
    when (focused /= Just w) (focus w)


moveMouseX, moveMouseY :: Word32 -> X ()
moveMouseX x = do (_, cy) <- getMousePosition
                  updateMousePosition (x, cy)
moveMouseY y = do (cx, _) <- getMousePosition
                  updateMousePosition (cx, y)


moveMouseX', moveMouseY' :: Word32 -> X ()
moveMouseX' x = do (cx, _, cw, _) <- currentScreenRect
                   moveMouseX (cx + cw - x)
moveMouseY' y = do (_, cy, _, ch) <- currentScreenRect
                   moveMouseY (cy + ch - y)


moveMouseRel :: D -> X ()
moveMouseRel (x, y) = do (cx, cy) <- getMousePosition
                         updateMousePosition (cx + x, cy + y)


moveMouseWindowCenter :: Window -> X ()
moveMouseWindowCenter w = do (cx, cy, cw, ch) <- windowRect w
                             updateMousePosition (cx + cw `div` 2, cy + ch `div` 2)


moveMouseWindowCenter' :: X ()
moveMouseWindowCenter' = withFocused moveMouseWindowCenter


mouseClick :: Int -> X ()
mouseClick button = safeSpawn "xdotool" ["click", show button]


mouseClickWithNoMod :: Int -> X ()
mouseClickWithNoMod button = safeSpawn "xdotool" ["click", "--clearmodifiers", show button]
