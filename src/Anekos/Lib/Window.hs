
module Anekos.Lib.Window (
    currentScreenRect,
    decorateName,
    enlargeWindow,
    moveFocusedWindowX,
    moveFocusedWindowX',
    moveFocusedWindowY,
    moveFocusedWindowY',
    resizeFocusedWindow,
    resizeWindowPrompt,
    snapWindow,
    updateWindowPosition,
    updateWindowPosition',
    windowRect
) where


import Anekos.Lib.CommandMaker (TitledPrompt(..))
import Anekos.Lib.XUtils (getCurrentScreen)

import qualified Data.Set as S
import Graphics.X11.Xinerama (getScreenInfo)
import XMonad
import XMonad.Prompt (XPConfig(autoComplete), mkXPromptWithReturn, mkComplFunFromList')
import qualified XMonad.StackSet as SS
import XMonad.Hooks.ManageDocks (calcGap)
import XMonad.Util.NamedWindows (getName)
import XMonad.Util.XUtils (fi)
import XMonad.Util.Types (Direction2D(..))
import Control.Applicative ((<$>))
import Control.Monad (join)
import Data.Char (toLower, isDigit)
import qualified Data.Set as S
import Data.Text (split, pack, unpack)
import GHC.Word (Word32)


type R = (Dimension, Dimension, Dimension, Dimension)


windowRect :: Window -> X R
windowRect w = do
    XConf { display = dpy } <- ask
    wa2rect <$> io (getWindowAttributes dpy w)


currentScreenRect :: X R
currentScreenRect = do
    Rectangle {
      rect_x = sx, rect_y = sy, rect_width = sw, rect_height = sh
    } <- gets (screenRect . SS.screenDetail . SS.current . windowset)
    return (fi sx, fi sy, sw, sh)


-- ref: contrib/Actions/FloatKeys

resizeWindow' :: Display -> Window -> ((Word32, Word32) -> (Word32, Word32)) -> X ()
resizeWindow' dpy wnd f = do
    io $ raiseWindow dpy wnd
    wa <- io $ getWindowAttributes dpy wnd
    let (w, h) = f (fromIntegral (wa_width wa), fromIntegral (wa_height wa))
    io $ resizeWindow dpy wnd w h
    io $ sync dpy True


-- FIXME Not Works?
resizeFocusedWindow :: ((Word32, Word32) -> (Word32, Word32)) -> X ()
resizeFocusedWindow f = withDisplay $ \d -> withFocused $ \w -> resizeWindow' d w f


resizeFocusedWindow' :: ((Word32, Word32) -> X (Maybe (Word32, Word32))) -> X ()
resizeFocusedWindow' f = withDisplay $ withFocused . resize
  where
    resize d w = do
        io $ raiseWindow d w
        wa <- io $ getWindowAttributes d w
        wh <- f (fromIntegral (wa_width wa), fromIntegral (wa_height wa))
        whenJust wh $ io . uncurry (resizeWindow d w)


resizeWindowPrompt :: XPConfig -> X ()
resizeWindowPrompt xpc = withFocused $ \wnd -> resizeFocusedWindow' $ prompt wnd
  where
    prompt wnd (w1, h1) = do
        sizeCandidates' <- sizeCandidates wnd
        let caption = "Window Size (" ++ show w1 ++ "," ++ show h1 ++ ")"
        ret <- mkXPromptWithReturn (TitledPrompt caption)
                                   xpc { autoComplete = Nothing }
                                   (mkComplFunFromList' def $ predefinedSizeList ++ sizeCandidates')
                                   (return . parseSizeText)
        return $ join ret


sizeCandidates :: Window -> X [String]
sizeCandidates w = do
    (_, _, w, h) <- windowRect w
    let list' = list w h
    return $ list' "*" (*) [2, 3, 4, 5] ++ list' "/" div [2, 3, 4, 5]
  where
    list w h sep f ps = map (toText sep . power f w h) ps
    power f w h p = (w `f` p, h `f` p, p)
    toText sep (w, h, p) = show w ++ "x" ++ show h ++ sep ++ show p



parseSizeText :: String -> Maybe (Word32, Word32)
parseSizeText s = set $ map (read . unpack) $ take 2 $ split (not . isDigit) (pack s)
  where
    set (w:h:[]) = Just (w, h)
    set _        = Nothing


predefinedSizeList :: [String]
predefinedSizeList = ["100x100", "1024x768", "1280x960", "3840x2160"]


wa2rect :: WindowAttributes -> R
wa2rect wa = ( fi (wa_x wa)
             , fi (wa_y wa)
             , fi (wa_width wa)
             , fi (wa_height wa) )


enlargeWindow :: Window -> X ()
enlargeWindow wnd = do
    XConf { display = dpy } <- ask
    (_, _, w, h) <- currentScreenRect
    let w' = w `percent` 95
        h' = h `percent` 95
    updateWindowPosition wnd ((w - w') `div` 2, (h - h') `div` 2)
    io $ resizeWindow dpy wnd w' h'
    io $ sync dpy True
  where
    percent x p = x * p `div` 100


updateWindowPosition :: Window -> D -> X ()
updateWindowPosition w (x, y) =
    withDisplay $ \d -> do
      io $ moveWindow d w (fi x) (fi y)
      float w


updateWindowPosition' :: Window -> (Rational, Rational) -> X ()
updateWindowPosition' w (rx, ry) = do
    (cx, cy, cw, ch) <- windowRect w
    (sx, sy, sw, sh) <- currentScreenRect
    let dx = ceiling $ toRational sx + toRational (sw - cw) * rx
        dy = ceiling $ toRational sy + toRational (sh - ch) * ry
    updateWindowPosition w (dx, dy)


moveWindowX, moveWindowY :: Word32 -> Window -> X ()
moveWindowX x w = do (sx, _, _, _) <- currentScreenRect
                     (_, cy, _, _) <- windowRect w
                     updateWindowPosition w (sx + x, cy)
moveWindowY y w = do (_, sy, _, _) <- currentScreenRect
                     (cx, _, _, _) <- windowRect w
                     updateWindowPosition w (cx, sy + y)


moveFocusedWindowX, moveFocusedWindowY :: Word32 -> X ()
moveFocusedWindowX = withFocused . moveWindowX
moveFocusedWindowY = withFocused . moveWindowY


-- | move focues window relative
moveFocusedWindowX', moveFocusedWindowY' :: Word32 -> X ()
moveFocusedWindowX' x = do (sx, _, sw, _) <- currentScreenRect
                           withFocused $ \w -> do
                             (_, cy, cw, _) <- windowRect w
                             updateWindowPosition w (sx + sw - x - cw, cy)
moveFocusedWindowY' y = do (_, sy, _, sh) <- currentScreenRect
                           withFocused $ \w -> do
                             (cx, _, _, ch) <- windowRect w
                             updateWindowPosition w (cx, sy + sh - y - ch)


decorateName :: WindowSpace -> Window -> X String
decorateName ws w = do
    name <- fmap (map toLower . show) $ getName w
    return $ name ++ " [" ++ SS.tag ws ++ "]"


snapWindow :: Direction2D -> Window -> X ()
snapWindow dir w = do
    (wx, wy, ww, wh) <- windowRect w
    dpy <- asks display
    gaps <- calcGap $ S.fromList [L, D, U, R]
    sid <- getCurrentScreen
    si <- gaps . (!! fromIntegral sid) <$> io (getScreenInfo dpy)
    let rx = fi $ rect_x si
        ry = fi $ rect_y si
        rw = rect_width si - ww + rx
        rh = rect_height si - wh + ry
    case dir of
      L -> updateWindowPosition w (rx, wy)
      U -> updateWindowPosition w (wx, ry)
      R -> updateWindowPosition w (rw, wy)
      D -> updateWindowPosition w (wx, rh)
