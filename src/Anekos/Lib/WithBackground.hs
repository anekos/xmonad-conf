
{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, FlexibleContexts, DeriveDataTypeable #-}

module Anekos.Lib.WithBackground (
    WithBGMessage(..),
    withBackground
) where


import Data.List as L (filter)
import XMonad
import XMonad.Hooks.FadeInactive (setOpacity)
import XMonad.StackSet as SS
import XMonad.Layout.LayoutModifier (LayoutModifier(..), ModifiedLayout(..))


defaultOpacity :: Rational
defaultOpacity = 7/10


data WithBGMessage = SetBackground !Window
                   | RemoveBackground  deriving (Typeable)
instance Message WithBGMessage


data BGData = BGData { bWindow :: !Window
                     , bBorder :: !Dimension } deriving (Read, Show)


data Background a = Background (Maybe BGData) deriving (Read, Show)
instance LayoutModifier Background Window where
    modifyLayout (Background Nothing)    w r  = runLayout w r
    modifyLayout (Background (Just bgd)) w r  = applyBackground bgd w r

    modifierDescription _ = "Background"

    redoLayout (Background Nothing) _ _ wrs = do
        mapM_ (flip setOpacity 1 . fst) wrs
        return (wrs, Just $ Background Nothing)
    redoLayout (Background (Just bgd@BGData{bWindow = bwin})) _ _ wrs = do
        let ws = map fst wrs
        if bwin `elem` ws
          then do mapM_ (`setOpacity` defaultOpacity) $ L.filter (/= bwin) ws
                  fixBackground bwin
                  return (wrs, Just $ Background $ Just bgd)
          else return (wrs, Just $ Background Nothing)

    handleMess (Background bgd) m
        | Just (SetBackground bwin') <- fromMessage m = do
            bgd' <- changeBackground bgd bwin'
            return (Just $ Background $ Just bgd')
        | Just RemoveBackground <- fromMessage m = do
            whenJust bgd toForeground
            return (Just $ Background Nothing)
        | otherwise = return Nothing


withBackground :: LayoutClass l a => l a -> ModifiedLayout Background l a
withBackground = ModifiedLayout $ Background Nothing


applyBackground :: (LayoutClass l Window) =>
       BGData
    -> SS.Workspace WorkspaceId (l Window) Window
    -> Rectangle
    -> X ([(Window, Rectangle)], Maybe (l Window))
applyBackground b wksp r = do
  let bwin = bWindow b
      stk = SS.stack wksp
      fgs = stk >>= SS.filter (bwin /=)
      live = bwin `elem` SS.integrate' (SS.stack wksp)
      focused = SS.stack wksp >>= Just . SS.focus
  if live
    then do (wrs, l) <- runLayout (wksp {SS.stack = fgs}) r
            return $
              if focused == Just bwin
              then ((bwin, r) : wrs, l)
              else (wrs ++ [(bwin, r)], l)
    else runLayout wksp r


toForeground :: BGData -> X ()
toForeground BGData{bWindow = w, bBorder = b} = do
    XMonad.focus w
    setOpacity w defaultOpacity
    setBorder w b


fixBackground :: Window -> X ()
fixBackground w = setOpacity w 1 >> setBorder w 0


toBackground :: Window -> X BGData
toBackground w =
    withDisplay $ \d -> do
        WindowAttributes{wa_border_width = b} <- io $ getWindowAttributes d w
        fixBackground w
        withFocused $ windows . SS.sink
        windows SS.focusDown
        return BGData{bWindow = w, bBorder = fromIntegral b}


changeBackground :: Maybe BGData -> Window -> X BGData
changeBackground (Just bgd@BGData{bWindow = w}) w' | w == w' = return bgd
changeBackground bgd w = do
    whenJust bgd toForeground
    toBackground w


setBorder :: Window -> Dimension -> X ()
setBorder w b = withDisplay $ \d -> io $ setWindowBorderWidth d w b
