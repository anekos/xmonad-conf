
module Anekos.Lib.ScriptServer (
    Args,
    Commands,
    scriptSeverEventHook
) where


import Control.Monad (when)
import Data.Monoid (All(..))
import qualified Data.Map as M
import System.Directory (removeFile)
import XMonad


type Args = [String]

type Commands = M.Map String ([String] -> X ())


scriptSeverEventHook :: Commands -> Event -> X All
scriptSeverEventHook cmds (ClientMessageEvent {ev_message_type = mt, ev_data = dt}) = do
    dpy <- asks display
    a <- io $ internAtom dpy "XMONAD_RUN_SCRIPT" False
    when (mt == a && dt /= []) $
      runScript cmds (head dt)
    return (All True)
scriptSeverEventHook _ _ = return (All True)


runScript :: Show a => Commands -> a -> X ()
runScript cmds wid = do
    let fn = "/tmp/xmonad-script/" ++ show wid
    c <- io $ readFile fn
    mapM_ (runner . words) $ lines c
    io $ removeFile fn
  where
    runner []     = return ()
    runner (x:xs) = whenJust (M.lookup x cmds) ($ xs)
