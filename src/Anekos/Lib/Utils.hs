
module Anekos.Lib.Utils where


import XMonad (whenJust)
import Control.Applicative


infixl 4  <<$


(<<$) :: (Monad f, Functor f) => ((a -> f a) -> f a) -> (a -> b) -> f b
x <<$ y = y <$> x return


whenJustX :: (Monad m) => m (Maybe n) -> (n -> m ()) -> m ()
whenJustX x f = x >>= (flip whenJust) f
