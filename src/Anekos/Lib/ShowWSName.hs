
{-# LANGUAGE DeriveDataTypeable, NoMonomorphismRestriction #-}

module Anekos.Lib.ShowWSName (
    showWSNameHook
) where


import Anekos.Lib.FlashText (flashText, FTConfig(..))

import Prelude hiding (lookup)
import Control.Monad (when)
import Data.Ratio ((%))
import XMonad
import qualified XMonad.StackSet as SS
import qualified XMonad.Util.ExtensibleState as XS


newtype ShowWSName = ShowWSName (Maybe String) deriving (Read, Show, Typeable)

instance ExtensionClass ShowWSName where
    initialValue = ShowWSName Nothing


showWSNameHook :: FTConfig -> X ()
showWSNameHook c = do
    (ShowWSName prev ) <- XS.get
    current <- withWindowSet (return . Just . SS.currentTag)
    when (prev /= current) $ do
      whenJust current $ \current' -> flashText c { ftFade = 1%2 } current'
      XS.put $ ShowWSName current

