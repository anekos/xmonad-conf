
{-# LANGUAGE DeriveDataTypeable #-}

module Anekos.Lib.Fade (
    fadeInactiveLogHook',
    opacityPrompt,
    opacityPromptFocused,
    setFadeInActive,
    setFixedFadeWindow,
    toggleFadeInActive,
    toggleIgnoreWindowToFade,
    unsetFixedFadeWindow
) where


import Anekos.Config.Config
import Anekos.Lib.CommandMaker
import Anekos.Lib.FlashText (FTConfig, flashText)

import Control.Applicative ((<$>))
import Control.Monad (forM_, when)
import Data.List (delete, isInfixOf)
import Data.Ratio ((%))
import XMonad as X
import qualified XMonad.StackSet as SS
import XMonad.Hooks.FadeInactive (fadeOutLogHook, isUnfocused, setOpacity)
import XMonad.Prompt (XPConfig(..))
import XMonad.Prompt.Input (inputPrompt)
import XMonad.Util.ExtensibleState as XS


data FadeState = FadeState { fsEnabled :: Bool
                           , fsIgnores :: [Window]
                           , fsFixes :: [(Window, Rational)]
                           , fsInOpacity :: Rational
                           , fsOutOpacity :: Rational } deriving (Typeable, Read, Show)

instance ExtensionClass FadeState where
    initialValue = FadeState True [] [] (10/10) (8/10)
    extensionType = PersistentExtension


fadeIn' :: Window -> X ()
fadeIn' w = do
    FadeState { fsInOpacity = fsIn } <- XS.get
    setOpacity w fsIn

fadeOut' :: Window -> X ()
fadeOut' w = do
    FadeState { fsOutOpacity = fsOut } <- XS.get
    setOpacity w fsOut


fadeIf' :: Query Bool -> Rational -> Rational -> Query Rational
fadeIf' qry amtIn amtOut = qry >>= \b -> return $ if b then amtOut else amtIn


fadeInactiveLogHook' :: Query Bool -> X ()
fadeInactiveLogHook' q = do
    FadeState {
      fsEnabled = on,
      fsIgnores = is,
      fsFixes = fs,
      fsInOpacity = fsIn,
      fsOutOpacity = fsOut
    } <- XS.get
    when on $ fadeOutLogHook $ fadeIf' (isUnfocused <&&> q <&&> isNotIgnore is) fsIn fsOut
    forM_ fs $ uncurry setOpacity


update :: (Window -> X ()) -> Bool -> X ()
update act on =
    withFocused $ \w -> do
        ws <- X.gets $ filter (/= w) . SS.allWindows . windowset
        forM_ ws act
        XS.modify $ \fs -> fs { fsEnabled = on }


update' :: Maybe FTConfig -> (Window -> X ()) -> Bool -> X ()
update' ftc act on = update act on >> flashText' ftc (text on)
  where
    text True = "on"
    text _    = "off"


setFadeInActive :: Bool -> X ()
setFadeInActive False = update fadeIn' False
setFadeInActive True  = update fadeOut' False


toggleFadeInActive :: Maybe FTConfig -> X ()
toggleFadeInActive ftc = XS.get >>= modi
  where
    modi (FadeState {fsEnabled = True }) = update' ftc fadeIn' False
    modi _                               = update' ftc fadeOut' True


toggleIgnoreWindowToFade :: Window -> X ()
toggleIgnoreWindowToFade w = XS.modify toggle
  where
    toggle fs@(FadeState { fsIgnores = is })
      | w `elem` is = fs { fsIgnores = delete w is }
      | otherwise   = fs { fsIgnores = w : is }


deleteFixedWindow :: Window -> [(Window, Rational)] -> [(Window, Rational)]
deleteFixedWindow w = filter ((w /=) . fst)


setFixedFadeWindow :: Rational -> Window -> X ()
setFixedFadeWindow r w = XS.modify toggle
  where
    toggle st@(FadeState { fsFixes = fs }) = st { fsFixes = (w, r) : deleteFixedWindow w fs }


unsetFixedFadeWindow :: Window -> X ()
unsetFixedFadeWindow w = XS.modify rm
  where
    rm st@(FadeState { fsFixes = fs }) = st { fsFixes = deleteFixedWindow w fs }


isNotIgnore :: [Window] -> Query Bool
isNotIgnore is = (`notElem` is) <$> ask


opacityPromptFocused :: XPConfig -> Maybe FTConfig -> X ()
opacityPromptFocused xpc ftc = do
    let xpc' = xpc { autoComplete = Just 0, searchPredicate = spred }
        compl = [(x, set' x) | x <- "reset" : "hide" : map show [1 :: Int ..10]]
    mkCommandPrompt xpc' "Opactiy (?%10)" compl
    refresh
  where
    set' "reset" = do withFocused $ \w -> unsetFixedFadeWindow w >> setOpacity w 1
                      flashText' ftc "reset"
    set' "hide"  = do withFocused $ setFixedFadeWindow 0
                      flashText' ftc "hide"
    set' num     = do withFocused $ setFixedFadeWindow $ read num % 10
                      flashText' ftc num
    spred        = isInfixOf


flashText' :: Maybe FTConfig -> String -> X ()
flashText' ftc s = whenJust ftc $ flip flashText $ "Fade: " ++ s

opacityPrompt :: XPConfig -> X ()
opacityPrompt xpc = do
    input <- inputPrompt xpc "Opacity for Active window (100%)"
    whenJust input (setIn . read . unempty "100%100")
    input <- inputPrompt xpc "Opacity for Inactive window (80%)"
    whenJust input (setOut . read . unempty "80%100")
  where
    unempty d "" = d
    unempty d x  = x ++ "%100"
    setIn x  = XS.modify $ \fs -> fs { fsInOpacity = x }
    setOut x = XS.modify $ \fs -> fs { fsOutOpacity = x }
