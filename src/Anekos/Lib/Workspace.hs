
{-# LANGUAGE ScopedTypeVariables #-}

module Anekos.Lib.Workspace (
    WorkspaceId,
    WorkspaceNumber,
    addWorkspace,
    goShyScreen,
    leaveShyScreen,
    modifyWorkspaceName,
    moveTo,
    nGreedyView,
    nShift,
    otherWorkspaces,
    renameWorkspacePrompt,
    selectWorkspacePrompt,
    shiftScreen,
    shiftWorkspace,
    sortWorkspaces,
    swapWorkspacePrompt,
    updateWorkspaceId,
    validWS,
    validWSName,
    viewEmptyWorkspace',
    viewNextScreen,
    viewNthScreen,
    viewPrevScreen,
    withNthWorkspace,
    workspaceKeys,
    workspaceNames,
) where


import Anekos.Lib.CommandMaker (mkCommandPrompt)
import Anekos.Lib.Mouse (getMousePosition)
import Anekos.Lib.Window (currentScreenRect)

import Control.Monad (when, forM, forM_)
import Data.Default (def)
import Data.Either (rights)
import Data.List (isPrefixOf, find, sort, sortBy)
import Data.Maybe (isJust, isNothing, listToMaybe)
import Text.ParserCombinators.Parsec
import XMonad
import XMonad.Actions.CycleWS (WSType(..), nextScreen, prevScreen, doTo, Direction1D(..))
import XMonad.Actions.LinkWorkspaces (defaultMessageConf, switchWS)
import XMonad.Actions.OnScreen (greedyViewOnScreen)
import XMonad.Actions.SwapWorkspaces (swapWithCurrent)
import XMonad.Actions.Warp (warpToScreen)
import XMonad.Prompt (XPConfig)
import XMonad.Prompt.Input (inputPrompt)
import qualified XMonad.Actions.DynamicWorkspaces as D
import qualified XMonad.Actions.PhysicalScreens as PhyScr
import qualified XMonad.StackSet as SS


-- WorkspaceId == (WorkspaceNumber ":" WorkspaceName)
type WorkspaceNumber = Int
type WorkspaceName = WorkspaceId


type ModifyWindowSet = WindowSet -> WindowSet


validWSName :: String -> Bool
validWSName "NSP"           = False
validWSName name
  | "Shy" `isPrefixOf` name = False
  | otherwise               = True


validWS :: WSType
validWS = WSIs $ return validWS'


validWS' :: SS.Workspace WorkspaceId l a -> Bool
validWS' w = validWSName (SS.tag w) && isJust (SS.stack w)
  where


findEmptyWorkspace :: SS.StackSet WorkspaceId l a s sd -> Maybe (SS.Workspace WorkspaceId l a)
findEmptyWorkspace = find (isNothing . SS.stack) . SS.workspaces


withEmptyWorkspace :: (WorkspaceId -> X ()) -> X ()
withEmptyWorkspace f = do
    ws <- gets windowset
    whenJust (findEmptyWorkspace ws) (f . SS.tag)


viewEmptyWorkspace' :: X ()
viewEmptyWorkspace' = withEmptyWorkspace (windows . SS.view)


sortWorkspaces :: Ord i => [SS.Workspace i l a] -> [SS.Workspace i l a]
sortWorkspaces = sortBy comp
  where
    comp x y = compare (SS.tag x) (SS.tag y)


nWorkspaceId :: WorkspaceNumber -> WindowSet -> Maybe WorkspaceId
nWorkspaceId n ws = find (isPrefixOf (wkspNum n)) names
  where
    names = map SS.tag $ SS.workspaces ws


-- num :: (WorkspaceId -> ModifyWindowSet) -> WorkspaceNumber -> ModifyWindowSet
-- num f n = \ws -> maybe ws (flip f $ ws) $ nWorkspaceId n ws


withNthWorkspace :: (WorkspaceId -> ModifyWindowSet) -> WorkspaceNumber -> X ()
withNthWorkspace f n = do
    ws <- gets windowset
    case nWorkspaceId n ws of
      (Just n') -> exec n'
      Nothing   -> let n' = wkspNum n
                   in  D.addWorkspace n' >> exec n'
  where
    exec n' = windows $ f n'


-- | Go N th screen.
shiftScreen :: ScreenId -> ModifyWindowSet
shiftScreen sid ws =
    let to = listToMaybe $ filter ((== sid) . SS.screen) $ SS.visible ws
    in maybe ws shift to
  where
    shift s = let i = (SS.tag . SS.workspace) s
              in SS.shift i ws


-- | Go N th workspace.
shiftWorkspace :: WorkspaceNumber -> ModifyWindowSet
shiftWorkspace n ws = maybe ws (shift . SS.tag) found
  where
    n'          = wkspNum n
    found       = listToMaybe $ filter (isPrefixOf n' . SS.tag) $ SS.workspaces ws
    shift name  = SS.shift name ws


-- | Make workspace name by number and name.
mkWSId :: WorkspaceNumber -> WorkspaceName -> WorkspaceId
mkWSId i "" = wkspNum i
mkWSId i n  = wkspNum i ++ ":" ++ n


parseWSId :: Parser (WorkspaceNumber, WorkspaceName)
parseWSId = do i <- many1 digit
               n <- wsName <|> empty
               return (read i, n)
  where
    wsName  = char ':' >> many1 anyChar
    empty   = return ""


-- | Extract workspace id to number and name.
extractWSId :: WorkspaceId -> Either ParseError (WorkspaceNumber, WorkspaceName)
extractWSId = parse parseWSId ""


workspaceNames :: [SS.Workspace WorkspaceId l a] -> [WorkspaceId]
workspaceNames = sort . map snd . validWorkspaces


validWorkspaceNumbers :: [SS.Workspace WorkspaceId l a] -> [WorkspaceNumber]
validWorkspaceNumbers = sort . filter (> 0) . map fst . validWorkspaces


validWorkspaces :: [SS.Workspace WorkspaceId l a] -> [(WorkspaceNumber, WorkspaceName)]
validWorkspaces =  rights . map (extractWSId . SS.tag)


newWorkspaceNumber :: [SS.Workspace WorkspaceId l a] -> WorkspaceNumber
newWorkspaceNumber = (+ 1) . length . takeWhile (uncurry (==)) . zip [1..] . validWorkspaceNumbers


-- | If given name workspace exists, return that workspace id.
newWorkspaceId :: WorkspaceName -> [SS.Workspace WorkspaceId l a] -> WorkspaceId
newWorkspaceId n ws =
    case find ((== n) . snd) $ validWorkspaces ws of
      (Just (i, n')) -> mkWSId i n'
      Nothing        -> mkWSId (newWorkspaceNumber ws) n


-- | Reject empty workspaces.
cleanWorkspaces :: ModifyWindowSet
cleanWorkspaces ws = ws { SS.hidden = filter (isJust . SS.stack) (SS.hidden ws) }


addWorkspace :: WorkspaceId -> X WorkspaceId
addWorkspace w = do
    windows cleanWorkspaces
    ws <- gets (SS.workspaces . windowset)
    let w' = newWorkspaceId w ws
    D.addWorkspace w' >> return w'


-- newWorkspaceName' :: (WorkspaceId -> ModifyWindowSet) -> WorkspaceId -> ModifyWindowSet
-- newWorkspaceName'  f n ws = let n' = newWorkspaceId n (SS.workspaces ws)
--                             in  f n' ws


updateWorkspaceId :: WorkspaceId -> ModifyWindowSet
updateWorkspaceId n ws =
    let settag w = w { SS.tag = n}
        setws s = s { SS.workspace = settag $ SS.workspace s }
        setscr st = st { SS.current = setws $ SS.current st }
    in  setscr ws


modifyWorkspaceName :: WorkspaceId -> WorkspaceName -> WorkspaceId
modifyWorkspaceName wid name
    | (Right (i, _)) <- extractWSId wid = mkWSId i name
    | otherwise                        = name


renameWorkspacePrompt :: XPConfig -> X ()
renameWorkspacePrompt xpc = do
    ws <- gets windowset
    let cur = SS.tag $ SS.workspace $ SS.current ws
    case extractWSId cur of
      (Left _)  -> prompt1
      (Right x) -> prompt2 x
  where
    prompt1               = D.renameWorkspace xpc
    prompt2 (i, n)        = do
        input <- inputPrompt xpc ("Workspace name (" ++ mkWSId i n ++ ")")
        whenJust input (windows . updateWorkspaceId . fix i)
    fix i n               =
        case extractWSId n of
          (Right _) -> n
          (Left _)  -> mkWSId i n


swapWorkspacePrompt :: XPConfig -> X ()
swapWorkspacePrompt xpc = do
    cws <- gets windowset
    let ct = SS.tag $ SS.workspace $ SS.current cws
        ts = sort $ filter (/= ct) $ map SS.tag $ filter validWS' $ SS.workspaces cws
    mkCommandPrompt xpc "Swap with" [(t, windows $ swapWithCurrent t) | t <- ts]
    return ()


nShift :: WorkspaceNumber -> X ()
nShift = withNthWorkspace SS.shift


nGreedyView :: WorkspaceNumber -> X ()
nGreedyView n = do
    ws <- gets windowset
    case nWorkspaceId n ws of
      (Just n') -> switchWS view defaultMessageConf n'
      Nothing   -> let n' = wkspNum n
                   in  D.addWorkspace n' >> view n'
  where
    view = windows . SS.greedyView


workspaceKeys :: [(String, X ())]
workspaceKeys =
    -- ウィンドウを指定ワークスペースに移動し、そのワークスペースに移動する
    [ ("M-S-" ++ show n, nShift n) | n <- [0 .. 9] ]
    ++
    -- ウィンドウに移動
    [ ("M-" ++ show n, nGreedyView n >> leaveShyScreen) | n <- [0 .. 9]]


selectWorkspacePrompt :: XPConfig -> X ()
selectWorkspacePrompt xpc = do
    ws <- gets $ sort . map SS.tag . filter validWS' . SS.workspaces . windowset
    mkCommandPrompt xpc "Workspace" [(w, windows $ SS.greedyView w) | w <- ws]
    return ()


wkspNum :: WorkspaceNumber -> String
wkspNum n
    | n == 0    = "10"
    | n < 10    = '0' : show n
    | otherwise = show n


viewPrevScreen, viewNextScreen :: X ()
viewPrevScreen = viewScreen prevScreen
viewNextScreen = viewScreen nextScreen


viewNthScreen :: PhyScr.PhysicalScreen -> X ()
viewNthScreen = viewScreen . PhyScr.viewScreen def


viewScreen :: X () -> X ()
viewScreen = id
-- ignore for updatePointer (LogHook.hs)
-- viewScreen view = do
--     (asx, asy, asw, ash) <- currentScreenRect
--     (cx, cy) <- getMousePosition
--     view
--     ws <- gets windowset
--     warpToScreen (SS.screen $ SS.current ws) (f (cx - asx) / f asw) (f (cy - asy) / f ash)
--     windows (const ws)
--   where
--     f = fromIntegral


-- | View the next/previous workspace.
moveTo :: Direction1D -> WSType -> X ()
moveTo dir t = doTo dir t (return sortWorkspaces) (windows . SS.greedyView)


otherWorkspaces :: SS.StackSet i l a s sd -> [SS.Workspace i l a]
otherWorkspaces ws = map SS.workspace (SS.visible ws) ++ SS.hidden ws


goShyScreen :: X ()
goShyScreen = do
    ss <- gets $ zip [0..] . map SS.screen . SS.screens . windowset
    forM_ ss $ \(i :: Integer, s) -> do
      let name = "Shy" ++ show i
      D.addHiddenWorkspace name
      windows $ greedyViewOnScreen s name


leaveShyScreen :: X ()
leaveShyScreen = do
    ss <- gets $ SS.screens . windowset
    let zss = zip [1..] ss
    inShys <- forM ss (return . isPrefixOf "Shy" . SS.tag . SS.workspace)
    when (or inShys) $ forM_ zss $ \(i, s) -> withNthWorkspace (greedyViewOnScreen $ SS.screen s) i
