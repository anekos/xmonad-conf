
{-# LANGUAGE DeriveDataTypeable, NoMonomorphismRestriction #-}

-- XXX XMonad.ShowText (flashText) は、マルチモニタ全体の中央に表示されてしまうので、オレオレ版を作った。

module Anekos.Lib.FlashText (
    flashText,
    flashTextOnScreen,
    handleTimerEvent,
    FTConfig(..)
) where


import Prelude hiding (lookup)
import Control.Applicative ((<$>))
import Control.Monad (when)
import Data.Default (Default, def)
import Data.Map (Map, empty, insert, lookup, delete)
import qualified Data.Map.Lazy as ML
import Data.Monoid (mempty, All)
import Data.Ratio ((%))
import XMonad
import XMonad.Hooks.FadeInactive (setOpacity)
import qualified XMonad.StackSet as SS
import XMonad.Util.Font (Align(AlignCenter), initXMF, releaseXMF, textExtentsXMF, textWidthXMF)
import XMonad.Util.Timer (startTimer)
import qualified XMonad.Util.ExtensibleState as XS
import XMonad.Util.XUtils (createNewWindow, deleteWindow, fi, showWindow, paintAndWrite)


data FTConfig = FTC
    { ftFont        :: String   -- ^ Font name
    , ftColor       :: String   -- ^ Font color
    , ftBgColor     :: String   -- ^ Background color
    , ftBorderColor :: String   -- ^ Border color
    , ftBorderWidth :: Int      -- ^ Border width
    , ftOpacity     :: Rational -- ^ Window opacity
    , ftPadding     :: Int      -- ^ Window padding
    , ftFade        :: Rational -- ^ Time in seconds of the name visibility
    } deriving (Read, Show)


instance Default FTConfig where
  def = FTC
    { ftFont         = "-misc-fixed-*-*-*-*-20-*-*-*-*-*-*-*"
    , ftColor        = "white"
    , ftBgColor      = "#285577"
    , ftBorderColor  = "#aaaaaa"
    , ftBorderWidth  = 1
    , ftOpacity      = 9%10
    , ftPadding      = 15
    , ftFade         = 1 }


data WindowInfo = WindowInfo
    { wiHandle :: Window
    , wiScreenId :: ScreenId
    , wiText :: String
    , wiBottom :: Int } deriving (Read, Show, Typeable)

newtype FlashText = FlashText (Map Atom WindowInfo) deriving (Read, Show, Typeable)

instance ExtensionClass FlashText where
    initialValue = FlashText empty


modFlashText :: (Map Atom WindowInfo -> Map Atom WindowInfo) -> FlashText -> FlashText
modFlashText f (FlashText m) = FlashText $ f m


finalizeWindow :: WindowInfo -> X ()
finalizeWindow = deleteWindow . wiHandle


-- | Handles timer events that notify when a window should be removed
handleTimerEvent :: Event -> X All
handleTimerEvent (ClientMessageEvent _ _ _ dis _ mtyp d) = do
    FlashText m <- XS.get
    a <- io $ internAtom dis "XMONAD_TIMER" False
    when (mtyp == a && not (null d)) $
         do let key = fromIntegral $ head d
            whenJust (lookup key m) finalizeWindow
            XS.put $ FlashText $ delete key m
    mempty
handleTimerEvent _ = mempty


flashText
  :: FTConfig -- ^ FTConfig
  -> String   -- ^ Display text
  -> X ()
flashText c s = gets (SS.current . windowset) >>= flashTextOnScreen c s


flashTextOnScreen
  :: FTConfig     -- ^ FTConfig
  -> String       -- ^ Display text
  -> SS.Screen i l a ScreenId ScreenDetail
  -> X ()
flashTextOnScreen c s scr = do
    FlashText wis <- XS.get
    let sid = SS.screen scr
        sd = SS.screenDetail scr
        wis' = ML.filter ((== SS.screen scr) . wiScreenId) wis
        same = ML.filter ((== s) . wiText) wis'
    when (ML.null same) $ do
      d <- asks display
      font <- initXMF $ ftFont c
      -- size
      let (Rectangle sx sy sw sh) = screenRect sd
      width <- (+ ftPadding c) <$> textWidthXMF d font s
      hight <- (+ ftPadding c) . fi <$> uncurry (+) <$> textExtentsXMF font s
      let y = fi sy + (fi sh - hight + 2) `div` 2
          y' = ML.foldr (\r acc -> max acc $ fi $ wiBottom r) y wis'
          x = fi sx + (fi sw - width + 2) `div` 2
          bottom = y' + fi hight + 10
      let r = Rectangle (fi x) (fi y') (fi width) (fi hight)
      -- window
      w <- createNewWindow r Nothing "" True
      setWindowAttrs d w
      showWindow w
      paintAndWrite w font (fi width) (fi hight) 0 (ftBgColor c) "" (ftColor c) (ftBgColor c) [AlignCenter] [s]
      -- finish
      releaseXMF font
      t <- startTimer $ ftFade c
      XS.modify $ modFlashText $ insert (fromIntegral t) (WindowInfo w sid s $ fi bottom)
  where
    setWindowAttrs d w = do
      io $ setWindowBorderWidth d w $ fi $ ftBorderWidth c
      bc <- io $ initColor d $ ftBorderColor c
      whenJust bc $ \bc' -> io $ setWindowBorder d w bc'
      setOpacity w $ ftOpacity c
