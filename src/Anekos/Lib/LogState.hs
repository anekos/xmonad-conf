
{-# LANGUAGE DeriveDataTypeable #-}

module Anekos.Lib.LogState (
  LogState(..),
  updateState,
  focusChanged
) where


import Anekos.Lib.XUtils

import Control.Applicative ((<$>), (<*>))
import Control.Monad (unless, when)
import Data.List ((\\))
import Data.Maybe (isJust)
import System.Random -- (getStdGen)
import XMonad
import XMonad.Util.ExtensibleState as XS



data LogState = LogState { lsStartup :: Bool
                         , lsFirst :: Bool
                         , lsFocus :: Maybe Window
                         , lsScreen :: Maybe ScreenId
                         , lsLayout :: Maybe String
                         , lsVisibleWindows :: [Window]
                         , lsAllWindows :: [Window]
                         , lsWorkspaces  :: [WorkspaceId]
                         } deriving (Typeable, Read, Show)

instance ExtensionClass LogState where
    initialValue = LogState { lsStartup = False
                            , lsFirst = True
                            , lsFocus = Nothing
                            , lsScreen = Nothing
                            , lsLayout = Nothing
                            , lsVisibleWindows = []
                            , lsAllWindows = []
                            , lsWorkspaces = []
                            }


updateState :: X ()
updateState = do
    vws <- getVisibleWindows
    aws <- getAllWindows
    f <- getFocus
    s <- getCurrentScreen
    ws <- getCurrentWorkspaces
    l <- getCurrentLayout
    XS.modify $ \ls -> ls { lsVisibleWindows = vws
                          , lsFocus = f
                          , lsScreen = Just s
                          , lsLayout = Just l
                          , lsAllWindows = aws
                          , lsWorkspaces = ws }


focusChanged :: X Bool
focusChanged = (/=) <$> (XS.gets lsFocus) <*> getFocus
