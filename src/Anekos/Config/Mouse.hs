
module Anekos.Config.Mouse (
    mouseStartup,
    myMouseBindings
) where


import Anekos.Config.Action
import Anekos.Lib.Workspace (validWS, moveTo)
import Anekos.Lib.XUtils (getCurrentLayout)

import Data.List (isSuffixOf)
import qualified Data.Map as M
import XMonad
import qualified XMonad.StackSet as SS
import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.MouseGestures (mouseGesture)
import XMonad.Hooks.FadeInactive (setOpacity)
import XMonad.Util.Types (Direction1D(..), Direction2D(..))


withFade :: (Window -> X ()) -> Window -> X ()
withFade f w = whenX (isClient w) $ setOpacity w (1/2) >> f w


myMouseBindings :: XConfig Layout -> M.Map (KeyMask, Button) (Window -> X ())
myMouseBindings (XConfig {modMask = mm}) = M.fromList
    [ ((mm, button3),  mouseGesture myGestures)
    , ((mm, button1),  focus >> withFade mouseMoveWindow)
    , ((ms, button1),  focus >> withFade mouseResizeWindow)
    , ((0,  8),        mouseGesture myGestures) ]
    where
      ms = mm .|. shiftMask


myGestures :: M.Map [Direction2D] (Window -> X ())
myGestures = M.fromList
    [ ([],          focus)
    , ([L],         const $ moveTo Prev validWS)
    , ([R],         const $ moveTo Next validWS)
    , ([U],         upGesture)
    , ([D],         downGesture)
    , ([D, R],      focusAnd kill1)
    , ([R, D],      focus >> windows . SS.sink) ]


focusAnd :: X () -> Window -> X ()
focusAnd x = (>> x) . focus


upGesture :: Window -> X ()
upGesture w = ifFull (focusUnmappedWindow' Next) (focus w >> windows SS.swapUp)


downGesture :: Window -> X ()
downGesture w = ifFull (focusUnmappedWindow' Prev) (focus w >> windows SS.swapDown)


inFull :: X Bool
inFull = getCurrentLayout >>= return . ("Full" `isSuffixOf`)


ifFull :: X () -> X () -> X ()
ifFull trueClause falseClause = do
  inFull' <- inFull
  if inFull'
  then trueClause
  else falseClause


mouseStartup :: X ()
mouseStartup = return ()
