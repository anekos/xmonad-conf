
{-# LANGUAGE NoMonomorphismRestriction, TypeSynonymInstances, MultiParamTypeClasses, DeriveDataTypeable, FlexibleContexts #-}

module Anekos.Config.Layout (
    doLayoutCommand,
    myLayoutHook,
    myLayoutNames,
    myLayoutPrompt,
    setLimitPrompt
) where


import Anekos.Config.Config
import Anekos.Config.Monitor (myMonitor)
import Anekos.Lib.CommandMaker (mkCommandPrompt, Cmd, doCommand')
import Anekos.Lib.WithBackground (withBackground)
import Anekos.Lib.VerticalSubGaps

import Control.Applicative ((<$>))
import Data.Char (isDigit)
import Data.List (isPrefixOf, sortBy, minimumBy, maximumBy)
import Data.Monoid (mempty)
import Data.Ord (comparing)
import Data.Ratio ((%))
import Data.Text (pack, unpack, split)
import Graphics.X11.Xinerama (getScreenInfo)
import XMonad hiding ((|||))
import qualified XMonad.StackSet as SS
import XMonad.Actions.MouseResize (mouseResize)
import XMonad.Hooks.ManageDocks (avoidStruts)
import XMonad.Layout.Decoration (Shrinker(..), CustomShrink(..), Theme(..))
import XMonad.Layout.Dishes (Dishes(..))
import XMonad.Layout.Gaps
import XMonad.Layout.IM (withIM)
import XMonad.Layout.ImageButtonDecoration (imageButtonDeco, defaultThemeWithImageButtons)
import XMonad.Layout.LayoutCombinators (JumpToLayout(..), (|||))
import XMonad.Layout.LayoutModifier (ModifiedLayout(..))
import XMonad.Layout.LayoutScreens (layoutScreens, fixedLayout)
import XMonad.Layout.LimitWindows (limitSlice, setLimit)
import XMonad.Layout.Magnifier (magnifier)
import XMonad.Layout.Maximize (maximize)
import XMonad.Layout.MessageControl (escape, unEscape)
import XMonad.Layout.Minimize (minimize)
import XMonad.Layout.Monitor (monitor)
import XMonad.Layout.Mosaic (Aspect(..), mosaic)
import XMonad.Layout.MosaicAlt (MosaicAlt(..))
import XMonad.Layout.MultiToggle (mkToggle, single, Toggle(..))
import XMonad.Layout.NoBorders (noBorders)
import XMonad.Layout.OneBig (OneBig(..))
import XMonad.Layout.PerWorkspace (onWorkspaces)
import XMonad.Layout.Reflect (reflectHoriz, REFLECTX(..), REFLECTY(..))
import XMonad.Layout.Renamed (renamed, Rename(Replace, CutWordsLeft))
import XMonad.Layout.ResizableTile (ResizableTall(..))
import XMonad.Layout.Sample (SampleLayout(..))
import XMonad.Layout.SimplestFloat (simplestFloat)
import XMonad.Layout.ShowMasterN (showMasterN)
import XMonad.Layout.Spiral (spiral)
import XMonad.Layout.StackTile (StackTile(..))
import XMonad.Layout.Tabbed (shrinkText, tabbed, addTabsAlways, tabbedAlways)
import XMonad.Layout.TwoPane (TwoPane)
import XMonad.Layout.Harakiri (Harakiri(..))
import XMonad.Layout.Reading (Reading(..))
import XMonad.Layout.MyWorkspaceDir (workspaceDir)
import XMonad.Prompt (XPConfig(..))
import XMonad.Prompt.Input (inputPrompt, (?+))
import XMonad.Util.WindowProperties (Property(..))


instance Shrinker CustomShrink where
    shrinkIt _ s = shrinkIt shrinkText clean
      where
        clean                           = take 100 s


setLimitPrompt :: X ()
setLimitPrompt = do
    input <- inputPrompt myXPConfig "Window limit"
    whenJust input (setLimit . read . def)
  where
    def "" = "1000"
    def x  = x


myLayoutNames' =
    [ "d-Dishes"
    , "f-Full"
    , "F-TrueFull"
    , "m-Magnifier"
    , "s-Spiral"
    , "S-Stack"
    , "t-Tall"
    , "l-Float"
    , "w-Wide"
    , "h-Harakiri"
    , "r-Reading"
    , "o-OneBig"
    , "x-Mosaic"
    , "X-MosaicAlt"
    , "n-Note"
    -- , ".i-IM"
    , ",x-ReflectX"
    , ",y-ReflectY"
    , ",s-Sample"
    , "!v-VGA"
    , "!s-SVGA"
    , "!x-XGA"
    , "!o-One"
    , "!i-Input"
    , "!r-Reset" ]


layoutActions :: [Cmd]
layoutActions = [ ("Sample",     sendMessage $ Toggle SampleLayout)
                , ("ReflectX",   sendMessage $ Toggle REFLECTX)
                , ("ReflectY",   sendMessage $ Toggle REFLECTY)
                , ("Reset",      rescreen)
                , ("One",        oneScreen)
                , ("VGA",        splitLargestScreen 640 480)
                , ("SVGA",       splitLargestScreen 800 600)
                , ("XGA",        splitLargestScreen 1024 768)
                , ("Input",      splitPrompt) ]


oneScreen :: X ()
oneScreen = do
    dpy <- asks display
    ss <- io $ getScreenInfo dpy
    let edge accr = maximumBy (comparing accr) ss
        right = edge rect_x
        bottom = edge rect_height
    layoutScreens 1 $ fixedLayout [ Rectangle 0 0
                                              (fi (rect_x right) + rect_width right)
                                              (fi (rect_y bottom) + rect_height bottom) ]


splitLargestScreen :: Dimension -> Dimension -> X ()
splitLargestScreen w h = do
    dpy <- asks display
    ss <- io $ getScreenInfo dpy
    let max = maximumBy sizeCmp ss
        ss' = concatMap (split max) ss
    layoutScreens (length ss') (fixedLayout ss')
  where
    r x y w h = Rectangle (fi x) (fi y) (fi w) (fi h)
    size r    = rect_width r * rect_height r
    sizeCmp   = comparing size
    split max m@(Rectangle mx my mw mh)
        | max == m  = filter ((> 0) . size) [ r mx my w h
                                            , r mx (fi my + h) w (mh - h)
                                            , r (fi mx + w) my (mw - w) mh ]
        | otherwise = [m]


splitPrompt :: X ()
splitPrompt = do
    input <- inputPrompt layoutXPConfig "Screen size (<WIDTH>x<HEIGHT>)"
    whenJust input $ \s ->
      case parseNums s of
        (w : h : _) -> splitLargestScreen w h
        _           -> return ()
  where
    parseNums = map (read . unpack) . split (not . isDigit) . pack


myLayoutNames = map (drop 1 . dropWhile (/= '-')) myLayoutNames'


noteGaps :: LayoutClass l a => l a -> ModifiedLayout VerticalSubGaps l a
noteGaps = verticalSubGaps (fi height) (fi height)
  where height = 280


-- 各種レイアウト
--    Tall      -> 縦に2分割
--    Wide      -> 横に2分割
--    Spiral    -> 所謂 フィボナッチレイアウト
--    Full      -> フルスクリーン
--    TreuFull  -> 完全なフルスクリーン (タブ無しでバーなどを隠す)
--    Magnifier -> Tall を基準に、フォーカスしているウィンドウだけを拡大
--    Mosaic    -> それぞれのウィンドウのサイズを調整可能、モザイク模様っぽくなる


named s = renamed [Replace s]



myLayoutHook = mkToggle (single SampleLayout) whole
  where
    whole       = limitSlice 100
                $ noBorders
                $ ModifiedLayout myMonitor
                $ renamed [CutWordsLeft 1] -- Remove "Prefix "
                $ minimize
                $ onWorkspaces ["01", "02", "03", "04", "05", "10", "Shy0", "NSP"] all_full
                $ all_wide
    all_full    = full ||| trueFull ||| wide |||tall |||
                  spiral' ||| magnifier' ||| float ||| mosaic' ||| mosaicAlt ||| reading ||| harakiri ||| dishes ||| stack ||| onebig ||| note
    all_wide    = wide |||tall ||| full ||| trueFull |||
                  spiral' ||| magnifier' ||| float ||| mosaic' ||| mosaicAlt ||| reading ||| harakiri ||| dishes ||| stack ||| onebig ||| note
    -- conf
    nmaster     = 1
    ratio       = 1/2
    delta       = 3/100
    -- inners
    tall        = named "Tall"        $ w $ m $ a $ t $ g   tall'
    wide        = named "Wide"        $ w $ m $ a $ t $ g $ Mirror tall'
    spiral'     = named "Spiral"      $ w $ m $ a $ t $ g $ spiral (1%1)
    full        = named "Full"        $ w $ m $ a         $ tabbedAlways CustomShrink myTabTheme
    trueFull    = named "TrueFull"    $ w $ m $ b           Full
    magnifier'  = named "Magnifier"   $ w $ m $ a $ t $ g $ magnifier tall'
    mosaic'     = named "Mosaic"      $ w $ m $ a $ t $ g $ mosaic 2 [3, 2]
    mosaicAlt   = named "MosaicAlt"   $ w $ m $ a $ t $ g $ MosaicAlt mempty
    float       = named "Float"       $ w $ m $ a           float'
    harakiri    = named "Harakiri"    $ w $ m $ a $ t $ g   Harakiri
    reading     = named "Reading"     $ w $ m $ a $ t $ g   Reading
    dishes      = named "Dishes"      $ w $ m $ a $ t $ g $ Dishes 2 (1/6)
    stack       = named "Stack"       $ w $ m $ a $ t $ g $ StackTile 1 (3/100) (1/4)
    onebig      = named "OneBig"      $ w $ m $ a $ t $ g $ OneBig (3/4) (3/4)
    note        = named "Note"        $ w $ a $ n $ t       Full
    -- detail
    tall'       = showMasterN myFTConfig nmaster $ ResizableTall nmaster delta ratio []
    float'      = imageButtonDeco shrinkText myButtonsTheme $ mouseResize simplestFloat
    -- modifier
    t           = addTabsAlways CustomShrink myTabTheme
    a           = myGaps -- . withBackground
    b           = id -- withBackground
    g           = mkToggle (single REFLECTX) . mkToggle (single REFLECTY)
    m           = maximize
    w           = workspaceDir "/tmp/xmosh"
    n           = noteGaps -- . withBackground



fi        = fromIntegral


jump :: Bool -> String -> X ()
jump inner l = if inner
                  then sendMessage $ escape $ JumpToLayout l
                  else sendMessage $ JumpToLayout l


layoutXPConfig :: XPConfig
layoutXPConfig  = myXPConfig { autoComplete = myAutoComplete, searchPredicate = isPrefixOf }


cutPrefix :: String -> String
cutPrefix = drop 1 . snd . break (== '-')


doLayoutCommand :: Bool -> String -> X ()
doLayoutCommand inner name = whenX (not <$> doCommand' layoutActions name)
                                   (jump inner name)


-- myLayoutPrompt -> レイアウト変更用のプロンプト。1文字入力するだけで決定される
myLayoutPrompt :: Bool -> X ()
myLayoutPrompt inner =
    mkCommandPrompt layoutXPConfig msg [(n, doLayoutCommand inner $ cutPrefix n) | n <- myLayoutNames']
  where
    msg | inner     = "Inner Layout"
        | otherwise = "Layout"

