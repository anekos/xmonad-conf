
{-# LANGUAGE NoMonomorphismRestriction,TupleSections #-}

module Anekos.Config.Action (
    clearFocusedWindowTty,
    copyFocusedWindowCwd,
    copyFocusedWindowId,
    copyFocusedWindowTty,
    copyFocusedWindowPid,
    copyPrompt,
    copyToAll,
    displayScreenName,
    displayWindowName,
    displayWSName,
    focusNext,
    focusUnmappedWindow',
    focusUnmappedWindow,
    keypress,
    keypressPrompt,
    makeImportantWindow,
    maximizeFocusedWindow,
    modifyClipboard,
    openSelectionPrompt,
    pullWindowPrompt,
    pushWindowPrompt,
    refreshWindow,
    removeCursor,
    rotateScreens,
    screenshot,
    setCurrentDirectory',
    setCurrentDirectoryPrompt,
    setRxvtFontName,
    setRxvtFontNamePrompt,
    setRxvtFontSize,
    setRxvtFontSizePrompt,
    setWindowRolePrompt,
    toggleBorder,
    updatePointer,
    xmsg
) where


import Anekos.Config.Config
import Anekos.Config.Scratchpad (scratchpads)
import Anekos.Lib.CommandMaker (mkCommandPrompt)
import Anekos.Lib.FlashText (flashText, flashTextOnScreen, FTConfig(..))
import Anekos.Lib.IO (commandResult, commandResult1)
import Anekos.Lib.LogState (focusChanged)
import Anekos.Lib.Mouse (updateMousePosition, mouseClickWithNoMod)
import Anekos.Lib.Notify (notify)
import Anekos.Lib.Window (decorateName, windowRect, resizeFocusedWindow)
import Anekos.Lib.Workspace (otherWorkspaces, shiftWorkspace, WorkspaceNumber(..))
import Anekos.Lib.Utils
import Anekos.Lib.XUtils

import Control.Applicative ((<$>), (<*>))
import Control.Arrow ((***), (&&&))
import Control.Concurrent (threadDelay)
import Control.Monad (when, filterM, forM_, (>=>), unless, join)
import qualified Data.Map as M
import Data.List (sortBy)
import Data.List.Split (splitOn)
import Data.Maybe (isJust, fromMaybe)
import Safe
import System.Directory (getHomeDirectory, getCurrentDirectory, setCurrentDirectory, createDirectoryIfMissing)
import System.FilePath.Posix (joinPath)
import XMonad
import XMonad.Actions.CopyWindow (copy)
import XMonad.Actions.Navigation2D (switchLayer)
import XMonad.Actions.Promote (promote)
import XMonad.Actions.WindowBringer (bringWindow)
import XMonad.Actions.Warp (banishScreen, Corner(LowerRight))
import XMonad.Layout.Maximize (maximizeRestore)
import XMonad.Layout.MyWorkspaceDir (Chdir(Chdir))
import XMonad.Prompt (XPConfig(..))
import XMonad.Prompt.Directory (directoryPrompt)
import XMonad.Prompt.Input (inputPrompt, inputPromptWithCompl)
import XMonad.Prompt.Shell (getCommands)
import qualified XMonad.StackSet as SS
import XMonad.Util.NamedScratchpad (namedScratchpadAction)
import XMonad.Util.NamedWindows (getName)
import XMonad.Util.Run (safeSpawn)
import XMonad.Util.Types (Direction1D(..))
import XMonad.Util.WindowProperties (getProp32)
import XMonad.Util.XSelection (safePromptSelection)
import XMonad.Util.XUtils (fi)


-- Only visible Workspace version (ref: CopyWindow.hs)
copyToAll :: (Eq s, Eq a) => SS.StackSet WorkspaceId l a s sd -> SS.StackSet WorkspaceId l a s sd
copyToAll ss = foldr (copy . SS.tag) ss notNSP
  where
    notNSP    = filter (\w -> SS.tag w /= "NSP") notEmptyWS
    notEmptyWS = filter (isJust . SS.stack) allWS
    allWS      = (map SS.workspace . SS.visible) ss ++ SS.hidden ss


xmsg :: Show a => a -> X ()
xmsg s = safeSpawn "xmessage" ["-default", "okay", show s]


openSelectionPrompt :: X ()
openSelectionPrompt = do
    cs <- io getCommands
    mkCommandPrompt myXPConfig "Open with" [(c, safePromptSelection c) | c <- cs]


copyPrompt :: X ()
copyPrompt = do
    ws <- withWindowSet (return . map SS.tag . SS.workspaces)
    mkCommandPrompt myXPConfig "Copy To"
                    [(n, windows $ copy n) | n <- ws]


removeCursor :: X ()
removeCursor =
    withFocused $ \w -> do
      fx <- runQuery (className =? "Firefox") w
      when fx resetFirefoxCursor
      banishScreen LowerRight


waitASecond :: Float -> X ()
waitASecond = io . threadDelay . round . (* 1000000)


resetFirefoxCursor :: X ()
resetFirefoxCursor =
    withFocused $ \w -> do
      (cx, cy, _, ch) <- windowRect w
      updateMousePosition (cx + 10, cy + ch - 20)
      refresh
      mouseClickWithNoMod 1
      refresh
      waitASecond 0.5


pullWindowPrompt :: XPConfig -> X ()
pullWindowPrompt c = do
    wm <- otherWSWindowMap
    mkCommandPrompt c "Pull window"
                    [(n, windows $ bringWindow w) | (n, w) <- M.toList wm]


pushWindowPrompt :: XPConfig -> X ()
pushWindowPrompt c = do
    ws <- gets (map SS.tag . otherWorkspaces . windowset)
    mkCommandPrompt c "Push window"
                    [(w, windows $ SS.shift w) | w <- ws]


otherWSWindowMap :: X (M.Map String Window)
otherWSWindowMap = do
    ws <- gets windowset
    M.fromList `fmap` concat `fmap` mapM keyValuePairs (otherWorkspaces ws)
  where
    keyValuePairs ws = mapM (keyValuePair ws) $ SS.integrate' (SS.stack ws)
    keyValuePair ws w = flip (,) w `fmap` decorateName ws w


isMinimized :: Window -> X Bool
isMinimized w = do
    state <- getAtom "_NET_WM_STATE"
    mini <- getAtom "_NET_WM_STATE_HIDDEN"
    wstate <- fromMaybe [] <$> getProp32 state w
    return (fromIntegral mini `elem` wstate)


focusNext :: X ()
focusNext = do
    ss <- withWindowSet <<$ id
    mstack <- withWindowSet <<$ SS.stack . SS.workspace . SS.current
    whenJust mstack $ \stack -> do
      minis <- filterM isMinimized $ SS.integrate' mstack
      let next = SS.peek $
                    SS.delete (SS.focus stack) $
                    SS.modify mstack (const $ SS.filter (`notElem` minis) stack) ss
      whenJust next focus


focusUnmappedWindow :: Direction1D -> X ()
focusUnmappedWindow dir = do
    dpy <- asks display
    ms <- withWindowSet <<$ SS.stack . SS.workspace . SS.current
    whenJust ms $ \s -> do
      let filter' side = map fst . filter snd <$> mapM (isUnmapped dpy) (side s)
      ts <- flip <$> filter' SS.up <*> filter' SS.down
      whenJust (headMay ts) focus
  where
    flip a b
        | dir == Next        = b ++ reverse a
        | otherwise          = a ++ reverse b
    isUnmapped dpy w           = do wa <- io $ getWindowAttributes dpy w
                                    m <- isMinimized w
                                    return (w, wa_map_state wa == waIsUnmapped && not m)


focusUnmappedWindow' :: Direction1D -> X ()
focusUnmappedWindow' dir = do
    mf <- getFocus
    whenJust mf $ \f -> do
      fl <- isFloating f
      if fl
         then switchLayer
         else focusUnmappedWindow dir


displayWSName :: X ()
displayWSName = withWindowSet (flashText myFTConfig . SS.currentTag)


displayScreenName :: X ()
displayScreenName = getAllScreens >>= mapM_ (\s -> flashTextOnScreen myFTConfig (show $ SS.screen s) s)


displayWindowName :: X ()
displayWindowName = withFocused $ getName >=> (flashText ftc . show)
  where
     ftc = myFTConfig { ftFont = "xft:VL Gothic:pixelsize=16", ftFade = 3 }


canFTConfig = myFTConfig { ftFont = myFont 16 }


copyAndNotify :: String -> X ()
copyAndNotify s = do copyToClipboard s
                     flashText myFTConfig s


copyAndNotify' :: Show a => a -> X ()
copyAndNotify' s = do let s' = show s
                      copyToClipboard s'
                      flashText myFTConfig s'


copyFocusedWindowCwd :: X ()
copyFocusedWindowCwd = withFocused $ \w -> getWindowCwd w `whenJustX` copyAndNotify


copyFocusedWindowPid :: X ()
copyFocusedWindowPid = withFocused $ \w -> getWindowPid w `whenJustX` copyAndNotify'


copyFocusedWindowId :: X ()
copyFocusedWindowId = withFocused copyAndNotify'


copyFocusedWindowTty :: X ()
copyFocusedWindowTty = withFocused $ \w -> (io $ commandResult1 "get-window-tty" [show w]) >>= copyAndNotify


copyFocusedWindowTitle :: X ()
copyFocusedWindowTitle = withFocused $ \w -> do name <- getName w
                                                copyToClipboard' name
                                                flashText myFTConfig $ show name


refreshWindow :: X ()
refreshWindow = do
    resizeFocusedWindow $ \(w, h) -> (w + 1, h)
    refresh
    resizeFocusedWindow $ \(w, h) -> (w - 1, h)


setRxvtFontSize :: String -> Window -> X ()
setRxvtFontSize sz wid = do
    io $ print ["", sz, "0", show wid]
    safeSpawn "set-font" ["", sz, "0", show wid]
    focus wid


setRxvtFontName :: String -> Window -> X ()
setRxvtFontName name wid = do
    io $ print [name, "", "", show wid]
    safeSpawn "set-font" [name, "", "", show wid]
    focus wid


setRxvtFontSizePrompt :: XPConfig -> X ()
setRxvtFontSizePrompt xpc = do
    input <- inputPrompt xpc "Font size"
    whenJust input $ \input ->
      withFocused $ setRxvtFontSize input


setRxvtFontNamePrompt :: XPConfig -> X ()
setRxvtFontNamePrompt xpc = do
    names <- io $ commandResult "font-names" ["2"]
    let compl = map (\name -> (name, withFocused $ setRxvtFontName name)) names
    mkCommandPrompt xpc "Font name" compl


setCurrentDirectory' :: String -> X ()
setCurrentDirectory' dir = sendMessage $ Chdir dir


setCurrentDirectoryPrompt :: XPConfig -> X ()
setCurrentDirectoryPrompt xpc = directoryPrompt xpc "cd: " setCurrentDirectory'


setWindowRolePrompt :: XPConfig -> X ()
setWindowRolePrompt xpc =
    withFocused $ \w -> do
      currentRole <- runQuery (stringProperty "WM_WINDOW_ROLE") w
      input <- inputPrompt xpc $ "Window Role (" ++ currentRole ++ ")"
      whenJust input $ \input ->
        safeSpawn "xprop" ["-id", show w, "-f",  "WM_WINDOW_ROLE", "8s",  "-set", "WM_WINDOW_ROLE", input]


clearFocusedWindowTty :: X ()
clearFocusedWindowTty = do
    Just w <- getFocus
    io $ safeSpawn "xeclear" [show w]


toggleBorder :: Window -> X ()
toggleBorder w =
    withDisplay $ \d -> io $ do
        cw <- wa_border_width `fmap` getWindowAttributes d w
        setWindowBorderWidth d w (if cw == 0 then 1 else 0)


makeImportantWindow :: Window -> X ()
makeImportantWindow w = do
    s <- gets (SS.current . windowset)
    ss <- gets (SS.visible . windowset)
    let ws = head $ sortBy cmp (s : ss)
    windows $ shift ws . float . SS.focusWindow w
  where
    cmp x y = compare (SS.screen x) (SS.screen y)
    shift = flip SS.shiftWin w . SS.tag . SS.workspace
    float = SS.float w (SS.RationalRect 0.05 0.05 0.9 0.9)


maximizeFocusedWindow :: X ()
maximizeFocusedWindow = getCurrentLayout >>= action
  where
    action "OneBig" = promote
    action _        = withFocused (sendMessage . maximizeRestore)

modifyClipboard :: X ()
modifyClipboard = safeSpawn "clipboard-modify" []

screenshot :: X ()
screenshot = do
    io $ createDirectoryIfMissing True "/tmp/xmonad-screenshot"
    spawn $ "sleep 0.3 ; scrot --focused /tmp/xmonad-screenshot/%Y-%m-%d_%H-%M-%S.png"
    -- flashText myFTConfig "Screen shot"


updatePointer :: X ()
updatePointer = do
    ws <- gets windowset
    dpy <- asks display
    rect <- case SS.peek ws of
          Nothing -> return $ (screenRect . SS.screenDetail . SS.current) ws
          Just w  -> windowAttributesToRectangle `fmap` io (getWindowAttributes dpy w)
    root <- asks theRoot
    mouseIsMoving <- asks mouseFocused
    (_sameRoot,_,currentWindow,rootX,rootY,_,_,_) <- io $ queryPointer dpy root
    drag <- gets dragging
    changed <- focusChanged
    unless (mouseIsMoving
            || not changed
            || isJust drag
            || not (currentWindow `SS.member` ws || currentWindow == none)) $ let
      -- focused rectangle
      (rectX, rectY) = (rect_x &&& rect_y) rect
      (rectW, rectH) = (fi . rect_width &&& fi . rect_height) rect
      -- reference position, with (0,0) and (1,1) being top-left and bottom-right
      refX = lerp (fst refPos) rectX (rectX + rectW)
      refY = lerp (snd refPos) rectY (rectY + rectH)
      -- final pointer bounds, lerped *outwards* from reference position
      boundsX = join (***) (lerp (fst ratio) refX) (rectX, rectX + rectW)
      boundsY = join (***) (lerp (snd ratio) refY) (rectY, rectY + rectH)
      -- ideally we ought to move the pointer in a straight line towards the
      -- reference point until it is within the above bounds, but…
      in io $ warpPointer dpy none root 0 0 0 0
            (round . clip boundsX $ fi rootX)
            (round . clip boundsY $ fi rootY)
  where
    refPos = (0.95, 0.95)
    ratio = (0, 0)
    lerp r a b = (1 - r) * realToFrac a + r * realToFrac b
    clip (lower, upper) x = if x < lower
                            then lower
                            else if x > upper
                                 then upper
                                 else x
    windowAttributesToRectangle wa = Rectangle (fi (wa_x wa))
                                       (fi (wa_y wa))
                                       (fi (wa_width wa + 2 * wa_border_width wa))
                                       (fi (wa_height wa + 2 * wa_border_width wa))


keypress :: [String] -> X ()
keypress keys = safeSpawn "xdotool" $ "key" : keys


keypressPrompt :: XPConfig -> X ()
keypressPrompt xpc = inputPrompt xpc "keys" >>= flip whenJust (keypress . splitOn " ")


-- modifyScreens :: ([a] -> [a]) -> X ()
modifyScreens mod = do
    windows f
  where
    f ss = let ws = mod $ (SS.workspace $ SS.current ss) : (map SS.workspace $ SS.visible ss)
           in  ss { SS.current = (SS.current ss) { SS.workspace = head $ ws }
                  , SS.visible = zipWith g (SS.visible ss) $ tail ws }
    g s w = s { SS.workspace = w }


rotateScreens :: Bool -> X ()
rotateScreens = modifyScreens . f
  where
    f True = mod
    f _    = reverse . mod . reverse
    mod (x:xs) = xs ++ [x]


-- pasteText :: X ()
-- pasteText = do dpy <- asks display
--                waitModKeyRelease
--                withFocused $ \w -> do
--                  (cx, cy, _, ch) <- windowRect w
--                  updateMousePosition (cx + 10, cy + ch - 20)
--                io $ flush dpy
--                spawn "xdotool getwindowfocus key --window %1 click 2"
-- 
-- 
-- waitModKeyRelease = waitKeyRelease [xK_Alt_L, xK_Alt_R]
