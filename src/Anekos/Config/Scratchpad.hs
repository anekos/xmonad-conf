
module Anekos.Config.Scratchpad (
  scratchpadHook,
  scratchpadKeys,
  scratchpadAction,
  scratchpads
) where


import Anekos.Config.ManageHook
import Anekos.Config.Config (myEditor')
import Anekos.Lib.Query (shellName, wmIconName, wmRole)

import Control.Exception.Extensible (bracket, catch, SomeException(..))
import Data.Char (isUpper, toLower)
import Data.Maybe (fromMaybe)
import XMonad
import qualified XMonad.StackSet as SS
import XMonad.Util.NamedScratchpad ( NamedScratchpad (NS, name)
                                   , namedScratchpadManageHook, allNamedScratchpadAction, namedScratchpadAction
                                   , defaultFloating, nonFloating, customFloating )


scratchpads :: [NamedScratchpad]
scratchpads =
    [ NS "calc"
         "floating-term 'XMonad calc' -e mfc"
         (shellName "XMonad calc")
         (customFloating $ SS.RationalRect (5/6) (4/5) (1/6) (1/5))

    , NS "eitaro"
         "eitaro server gui"
         (wmRole =? "eitaro")
         (customFloating $ SS.RationalRect (3/4) 0 (1/4) (1/4))

    , NS "htop"
         "urxvt -fn 'xft:Migu 2M:size=10' -n htop-normal -e htop"
         (wmIconName =? "htop-normal")
         (customFloating $ SS.RationalRect (1/2) (2/3) (1/2) (1/3))

    , NS "Htop"
         "urxvt -fn 'xft:Migu 2M:size=10' -n htop-large -e htop"
         (wmIconName =? "htop-large")
         (customFloating $ SS.RationalRect (1/10) (1/10) (8/10) (8/10))

    , NS "lisp-sbcl"
         "xterm -e sbcl --load ~/.vim-temp/bundle/slimv.vim/slime/start-swank.lisp"
         (className =? "XTerm" <&&> stringProperty "WM_ICON_NAME" =? "sbcl")
         (customFloating $ SS.RationalRect (1/4) (1/6) (2/4) (4/6))

    , NS "mpc"
         "kitty --title mpc -e ncmpcpp"
         (title =? "mpc")
         (customFloating $ SS.RationalRect (1/6) (1/6) (4/6) (2/3))

    , NS "ranger"
         -- "XMONAD_RANGER=1 st -t 'XMonad Ranger' -f \"Courier New:size=12\" -e ranger ~/ --cmd='set update_title=False' --cmd='map q shell xc scratchpad ranger'"
         -- "XMONAD_RANGER=1 LC_ALL=ja_JP.UTF-8 mlterm --title 'XMonad Ranger' -e ranger ~/ --cmd='set update_title=False' --cmd='map q shell xc scratchpad ranger'"
         "XMONAD_RANGER=1 LC_ALL=ja_JP.UTF-8 floating-term 'XMonad Ranger' -e ranger ~/ --cmd='set update_title=False' --cmd='map q shell xc scratchpad ranger'"
         (shellName "XMonad Ranger")
         (customFloating $ SS.RationalRect (1/10) (1/10) (8/10) (8/10))

    , NS "shell"
         "XMONAD_SHELL=1 floating-term 'XMonad Shell'"
         (shellName "XMonad Shell")
         (customFloating $ SS.RationalRect (1/4) (1/6) (2/4) (4/6))

    , NS "witter"
         "tw"
         (wmRole =? "twitter")
         (customFloating $ SS.RationalRect (2/6) (1/6) (1/3) (2/3))

    , NS "zsh"
         "XMONAD_LAUNCHER=1 floating-term 'XMonad Launcher'"
         (shellName "XMonad Launcher")
         (customFloating $ SS.RationalRect (1/3) (1/3) (1/3) (1/3)) ]

    -- "u" は ("M-s u", focusUrgent) として使用している

scratchpadKeys :: [(String, X ())]
scratchpadKeys = concatMap f scratchpads
  where
    f NS{name = n} =
        [ ("M-t " ++ key p n, (scratchpadAction n) scratchpads n)
        | p <- prefixes ]
    key prefix n
      | isUpper (head n) = prefix ++ "S-" ++ [toLower $ head n]
      | otherwise           = prefix ++ [head n]
    prefixes = ["", "M-"]


scratchpadAction :: String -> [NamedScratchpad] -> String -> X ()
scratchpadAction "music" = allNamedScratchpadAction
scratchpadAction _       = namedScratchpadAction


scratchpadHook :: ManageHook
scratchpadHook = namedScratchpadManageHook scratchpads
