
module Anekos.Config.Env.Netbook where


import Anekos.Config.Env.Common

import XMonad
import XMonad.Util.Run (spawnPipe)


myDzenStatus = "dzen2 -dock -y 600 -w 824 -ta l " ++ myDzenStyle 14
myDzenInfo   = runConky "netbook" "info" "-x 824 -y 600 -w 200 -ta r" 14


myStartup :: X ()
myStartup = myDzenInfo
