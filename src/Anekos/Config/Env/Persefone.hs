
module Anekos.Config.Env.Persefone where


import Anekos.Config.Env.Common

import XMonad
import XMonad.Util.Run (spawnPipe)


myDzenStatus = "dzen2 -dock -y 768 -w 1116 -ta l " ++ myDzenStyle 14
myDzenInfo   = runConky "persefone" "info" "-x 1116 -y 768 -w 250 -ta r" 14


myStartup :: X ()
myStartup = myDzenInfo
