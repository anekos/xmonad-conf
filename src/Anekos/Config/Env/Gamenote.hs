
module Anekos.Config.Env.Gamenote where


import Anekos.Config.Env.Common

import XMonad
import XMonad.Util.Run (spawnPipe)


myDzenStatus = "dzen2 -dock -y 768 -w 1166 -ta l " ++ myDzenStyle 14
myDzenInfo   = runConky "gamenote" "info" "-x 1166 -y 768 -w 200 -ta r" 14


myStartup :: X ()
myStartup = myDzenInfo
