
module Anekos.Config.Env.Jisui where


import Anekos.Config.Env.Common

import XMonad
import XMonad.Util.Run (spawnPipe)


-- 1366x768
myDzenStatus = "dzen2 -dock -y 768 -w 1166 -ta l " ++ myDzenStyle 14
myDzenInfo   = runConky "jisui" "info" "-x 1166 -y 768 -w 200 -ta r" 14

myMainScreen :: ScreenId
myMainScreen = 0

myStartup :: X ()
myStartup = myDzenInfo
