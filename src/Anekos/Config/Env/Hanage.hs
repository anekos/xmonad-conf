
module Anekos.Config.Env.Hanage where


import Anekos.Config.Env.Common

import XMonad
import XMonad.Util.Run (spawnPipe)


myDzenStatus = "dzen2 -dock -y 768 -w 824 -ta l " ++ myDzenStyle 14
myDzenInfo   = runConky "hanage" "info" "-x 824 -y 768 -w 200 -ta r" 14


myStartup :: X ()
myStartup = myDzenInfo
