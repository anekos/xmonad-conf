
module Anekos.Config.Env.Main (
    myDzenStatus,
    myGaps,
    myMainScreen,
    myStartup
) where


import Anekos.Config.Env.Common
import Anekos.Lib.VerticalSubGaps

import XMonad
import XMonad.Layout.LayoutModifier (ModifiedLayout(..))
import XMonad.Util.XUtils (fi)


-- 4K + 2r

gap = 0
fontSize = 20
barHeight = 40


myDzenStatusY= 2160 - barHeight


-- on left display
myDzenStatus = "dzen2 -dock -x 0 -y " ++ show myDzenStatusY ++ " -w 3840 -ta l " ++ myDzenStyle fontSize

-- on right display
myDzenInfo   = runConky "main" "info" ("-x 3840 -y " ++ (show $ 3840 - 20) ++ " -w 2160 -ta c") fontSize


myMainScreen :: ScreenId
myMainScreen = 0


myStartup :: X ()
myStartup = myDzenInfo >> myDzenSub 3840 2160 fontSize


myGaps :: LayoutClass l a => l a -> ModifiedLayout VerticalSubGaps l a
myGaps = verticalSubGaps (fi barHeight) (fi barHeight)






-- FullHD x2 右縦置き
-- myDzenInfo   = runConky "main" "info" "-x 1240 -y 1920 -w 680 -ta r" 10
-- myDzenStatus = "dzen2 -dock -x 0 -y 1920 -w 1240 -ta l " ++ myDzenStyle 10
-- myDzenMPD    = runConky "main" "mpd"  "-x 1920 -y 1920 -w 1080 -ta c" 16

-- FullHD x2 右縦置き(ずらし)
-- gap = 500
-- height = 20
-- y = 1080 + gap - height
-- myDzenInfo   = runConky "main" "info" ("-x 1140 -y " ++ show y ++ " -w 780 -ta r") 10
-- myDzenStatus = "dzen2 -dock -x 0 -y " ++ show y ++ " -w 1140 -ta l " ++ myDzenStyle 10
-- myDzenMPD    = runConky "main" "mpd"  "-x 1920 -y 0 -w 1920 -ta c" 16

-- FullHD x2
-- myDzenInfo   = runConky "main" "info" "-x 1140 -y 1920 -w 780 -ta r" 10
-- myDzenStatus = "dzen2 -dock -x 0 -y 1920 -w 1140 -ta l " ++ myDzenStyle 10
-- myDzenMPD    = runConky "main" "mpd"  "-x 1920 -y 0 -w 1920 -ta c" 16

-- FullHD x 1680x1050
-- myDzenInfo   = runConky "main" "info" "-x 1240 -y 1680 -w 680 -ta r" 10
-- myDzenStatus = "dzen2 -dock -x 0 -y 1920 -w 1240 -ta l " ++ myDzenStyle 10
-- myDzenMPD    = runConky "main" "mpd"  "-x 1920 -y 1680 -w 1080 -ta c" 16
