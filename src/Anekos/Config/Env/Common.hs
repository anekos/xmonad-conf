
module Anekos.Config.Env.Common (
    myDzenStyle,
    myDzenSub,
    myFont,
    myHintColor,
    runConky
) where

import XMonad
import Control.Monad (void)
import System.FilePath (joinPath)
import XMonad.Util.Run (spawnPipe)


myFont :: Int -> String
myFont x = fullname "Noto Sans CJK JP" x
  where
    fullname name size = "xft:" ++ name ++ ":pixelsize=" ++ show size ++ ":hinting=true:hintstyle=hintslight:antialias=true:rgba=rgb:lcdfilter=lcdlight"


myDzenFont :: Int -> String
myDzenFont size = "Noto Sans CJK JP" ++ "-" ++ show size


myHintColor :: String
myHintColor = "#905858" -- "222222"


myDzenStyle :: Int -> String
myDzenStyle fontSize = " -h " ++ (show $ fontSize * 2) ++ " -fg 'white' -bg '#222222' -e 'button3=' -fn '" ++ myDzenFont fontSize ++ "'"


myDzenSub :: Int -> Int -> Int -> X ()
myDzenSub x w fontSize = do
    runConky "main" "sub" ("-x " ++ show x ++ " -w " ++ show w ) fontSize


mkConkyDzen :: String -> String -> Int -> String
mkConkyDzen conf opt fontSize = "conky -c " ++ conf
                                ++ " | dzen2 -dock " ++ opt ++ myDzenStyle fontSize


runConky :: String -> String -> String -> Int -> X ()
runConky env name opt fnt = do dir <- asks (cfgDir . directories)
                               let fp = joinPath [dir, "env", env, "conky", name]
                               void $ spawnPipe $ mkConkyDzen fp opt fnt
