
module Anekos.Config.Env.Syn (
    myDzenStatus,
    myMainScreen,
    myStartup,
    myGaps
) where


import Anekos.Config.Env.Common
import Anekos.Lib.VerticalSubGaps

import XMonad
import XMonad.Layout.LayoutModifier (ModifiedLayout(..))
import XMonad.Hooks.ManageDocks (avoidStruts, AvoidStruts)


barHeight = 20


myDzenStatus :: String
myDzenStatus = "dzen2 -dock -y 1080 -w 1310 -ta l " ++ myDzenStyle 14


myDzenInfo :: X ()
myDzenInfo   = runConky "syn" "info" "-x 1310 -y 1080 -w 610 -ta r" 14


myMainScreen :: ScreenId
myMainScreen = 1


myStartup :: X ()
myStartup = myDzenInfo >> myDzenSub 1920 1920 14
    -- nGreedyView 10
    -- viewNextScreen


myGaps :: LayoutClass l a => l a -> ModifiedLayout AvoidStruts l a
myGaps = avoidStruts
