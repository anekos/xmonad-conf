
module Anekos.Config.Monitor (
    myMonitor
) where

import XMonad
import XMonad.Layout.Monitor


myMonitor :: Monitor a
myMonitor = monitor
    { prop = ClassName "Cairo-clock" -- `And` Title "MacSlow's Cairo-Clock"
    , rect = Rectangle 0 0 150 150
    , persistent = True
    , opacity = 0.9
    , visible = True
    , name = "clock" }
