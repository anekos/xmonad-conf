
module Anekos.Config.ScriptServerCommand (
    myScriptServerCommands
) where


import Anekos.Config.Action (setCurrentDirectory', makeImportantWindow)
import Anekos.Config.Command (runCommand)
import Anekos.Config.Config (myGSConfig, myFont)
import Anekos.Config.Layout (myLayoutNames, doLayoutCommand)
import Anekos.Config.Scratchpad (scratchpads, scratchpadAction)
import Anekos.Lib.FlashText (flashText, FTConfig(..))
import Anekos.Lib.Notify (notify)
import Anekos.Lib.Workspace (withNthWorkspace, nShift)
import Anekos.Lib.ScriptServer (Commands, Args)

import qualified Data.Char as DC
import qualified Data.Map as M
import Data.List
import Safe (readMay)
import System.Directory (removeFile)
import XMonad
import XMonad.Actions.CycleWS (nextScreen)
import XMonad.Actions.DynamicWorkspaces (addWorkspace)
import XMonad.Actions.GridSelect (gridselect, withSelectedWindow)
import XMonad.Actions.Navigation2D (windowGo, Direction2D(..))
import XMonad.Actions.Promote (promote)
import XMonad.Actions.WindowBringer (bringWindow)
import qualified XMonad.Hooks.ToggleHook as TH
import qualified XMonad.StackSet as SS
import XMonad.Layout.LayoutCombinators (JumpToLayout(..))


myScriptServerCommands :: Commands
myScriptServerCommands = M.fromList
    [ ("notify",     notify . unwords)
    , ("layout",     changeLayout)
    , ("workspace",  viewWorkspace)
    , ("scratchpad", scratchpad)
    , ("focus",      focusWindow)
    , ("select",     selectSomething)
    , ("screen",     screenCommand)
    , ("command",    runPromptCommand)
    , ("hook-next",  hookNext)
    , ("shift",      shift)
    , ("go",         windowGo')
    , ("cd",         cd)
    , ("pull",       pull)
    , ("new-workspace", newWorkspace)
    , ("important",  makeImportantWindow')
    , ("flash",      flash)
    , ("promote",    const promote)
    , ("sink",       const sink)
    ]


changeLayout :: Args -> X ()
changeLayout (l:xs) = sendMessage $ JumpToLayout l
changeLayout _      = return ()


viewWorkspace :: Args -> X ()
viewWorkspace [wid]
    | isNumber wid = withNthWorkspace SS.greedyView (read wid)
    | otherwise    = do
        s <- gets windowset
        if SS.tagMember wid s
           then windows $ SS.greedyView wid
           else addWorkspace wid
viewWorkspace (wid:sid:_) = do
    w <- screenWorkspace $ fromIntegral $ read sid
    whenJust w (windows . SS.view)
    withNthWorkspace SS.greedyView (read wid)
viewWorkspace _           = return ()


newWorkspace :: Args -> X ()
newWorkspace [wid] = addWorkspace wid
newWorkspace _     = return ()


focusWindow :: Args -> X ()
focusWindow [wid] = focus $ read wid
focusWindow _     = return ()


scratchpad :: Args -> X ()
scratchpad [name] = scratchpadAction name scratchpads name
scratchpad _      = return ()


selectSomething :: Args -> X ()
selectSomething ["layout"]  = do
    sel <- gridselect myGSConfig [(l, l) | l <- myLayoutNames]
    whenJust sel (doLayoutCommand False)
selectSomething ["window"]  = withSelectedWindow (windows . SS.focusWindow) myGSConfig
selectSomething _           = return ()


screenCommand :: Args -> X ()
screenCommand  ["next"]  = nextScreen
screenCommand _          = return ()


runPromptCommand :: Args -> X ()
runPromptCommand [cmd] = runCommand cmd
runPromptCommand _     = return ()


shift :: Args -> X ()
shift [num] | Just num' <- readMay num = nShift num'
            | otherwise                = windows $ SS.shift num
shift _     = return ()


hookNext :: Args -> X ()
hookNext [name] = TH.hookNext name True
hookNext _      = return ()


windowGo' :: Args -> X ()
windowGo' ["left"]  = windowGo L True
windowGo' ["down"]  = windowGo D True
windowGo' ["up"]    = windowGo U True
windowGo' ["right"] = windowGo R True
windowGo' _         = return ()


cd :: Args -> X ()
cd [path] = setCurrentDirectory' path
cd _      = return ()


pull :: Args -> X ()
pull [wid] | isNumber wid = windows $ bringWindow $ read wid
pull _ = return ()


makeImportantWindow' :: Args -> X ()
makeImportantWindow' [wid] | isNumber wid = makeImportantWindow $ read wid
makeImportantWindow' _ = return ()


isNumber :: String -> Bool
isNumber = all DC.isNumber


flash :: Args -> X ()
flash (text:args) = do
    let cnf = flashArgs args
    whenJust cnf $ \cnf -> flashText cnf text


flashArgs :: Args -> Maybe FTConfig
flashArgs [] = Just def
flashArgs [timeout] | isNumber timeout =
    Just $ def { ftFont = myFont 14 , ftFade = fromIntegral $ read timeout }
flashArgs [timeout, size] | isNumber size = do
    cnf <- flashArgs [timeout]
    return $ cnf { ftFont = myFont $ fromIntegral $ read size }
flashArgs _ = Nothing


sink :: X ()
sink = withFocused $ windows . SS.sink
