
{-# LANGUAGE ParallelListComp #-}

module Anekos.Config.Keyboard (
    myKeys,
    myNav2d
) where


import Anekos.Config.Action
import Anekos.Config.Command (mainCommandPrompt)
import Anekos.Config.Config
import Anekos.Config.Layout (myLayoutPrompt)
import Anekos.Config.Scratchpad (scratchpads, scratchpadKeys)
import Anekos.Lib.Fade (setFixedFadeWindow, unsetFixedFadeWindow)
import Anekos.Lib.Resume (reload)
import Anekos.Lib.SubKeyMode (KeyModeTable, enterKeyMode, withCount)
import Anekos.Lib.Mouse ( mouseClick
                        , moveMouseX, moveMouseY
                        , moveMouseX', moveMouseY'
                        , moveMouseRel
                        , moveMouseWindowCenter' )
import Anekos.Lib.Script (runScript)
import Anekos.Lib.Window ( moveFocusedWindowX, moveFocusedWindowY
                         , moveFocusedWindowX', moveFocusedWindowY'
                         , updateWindowPosition, snapWindow
                         , windowRect )
import Anekos.Lib.Workspace ( validWS, viewEmptyWorkspace', selectWorkspacePrompt
                            , viewNextScreen, viewPrevScreen, moveTo )
import Anekos.Lib.XUtils (waitKeyRelease)

import Control.Monad (when)
import Data.Default (def)
import Data.Ratio ((%))
import XMonad
import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.CycleWS (nextScreen, prevScreen, shiftNextScreen)
import XMonad.Actions.DynamicWorkspaces (selectWorkspace)
import XMonad.Actions.FloatKeys (keysMoveWindow, keysResizeWindow)
import XMonad.Actions.HitAHint (showWindowHints)
import XMonad.Actions.Navigation2D (windowGo, Direction2D(..)
                                   , withNavigation2DConfig
                                   , Navigation2DConfig(..)
                                   , centerNavigation )
import XMonad.Actions.RotSlaves (rotAllUp, rotSlavesUp)
import XMonad.Actions.Promote (promote)
import XMonad.Actions.Warp (warpToScreen)
import XMonad.Hooks.UrgencyHook (focusUrgent)
import XMonad.Layout.Maximize (maximizeRestore)
import XMonad.Layout.ResizableTile (MirrorResize(..))
import XMonad.Prompt (XPConfig(..))
import XMonad.Prompt.AppLauncher (launchApp)
import XMonad.Prompt.Man (manPrompt)
import XMonad.Prompt.Window (windowPrompt, WindowPrompt(..), allWindows)
import qualified XMonad.StackSet as SS
import XMonad.Util.NamedScratchpad (namedScratchpadAction)
import XMonad.Util.Paste (sendKey)
import XMonad.Util.Types (Direction1D(Next, Prev))
import qualified Data.Map as M
import System.Exit (exitSuccess)


-- "-S" for window or focus, "-C" for others.
--    rest:
--      --: a c d g v x
--      S-: a d e f m n p q s u v w x y z
myKeys :: [(String, X ())]
myKeys =
    -- Launcher
    [ ("M-i",               namedScratchpadAction scratchpads "zsh")
    , ("M-s",               namedScratchpadAction scratchpads "shell")
    , ("M-r",               namedScratchpadAction scratchpads "ranger")
    -- , ("M-S-<Return>",      spawn myTerminal)
    -- , ("M-S-g",             spawn myEditor)
    -- Window
    , ("M-<Return>",        promote)
    , ("M-b",               windowPrompt myXPConfig { autoComplete = Just 0 } Goto allWindows)
    , ("M-f",               showWindowHints myHAHConfig focus)
    , ("M-S-r",             rotAllUp)
    , ("M-S-c",             kill1)
    , ("M-S-C-c",           kill)
    , ("M-S-<Delete>",      kill1)
    , ("M-S-C-<Delete>",    kill)
    , ("M-S-h",             screenWorkspace 0 >>= flip whenJust (windows . SS.view))
    , ("M-S-j",             windows SS.swapDown)
    , ("M-S-k",             windows SS.swapUp)
    , ("M-S-l",             screenWorkspace 1 >>= flip whenJust (windows . SS.view))
    -- Workspace
    , ("M-h",               windowGo L True)
    , ("M-j",               windowGo D True)
    , ("M-k",               windowGo U True)
    , ("M-l",               windowGo R True)
    , ("M-o",               focusUnmappedWindow' Next)
    , ("M-S-o",             focusUnmappedWindow' Prev)
    , ("M-S-b",             selectWorkspacePrompt myXPConfig { autoComplete = Just 0 })
    , ("M-p",               moveTo Prev validWS)
    , ("M-n",               moveTo Next validWS)
    -- Resize
    , ("M-C-h",             sendMessage Shrink)
    , ("M-C-j",             sendMessage MirrorShrink)
    , ("M-C-k",             sendMessage MirrorExpand)
    , ("M-C-l",             sendMessage Expand)
    -- Screen
    , ("M-w",               viewPrevScreen)
    , ("M-e",               viewNextScreen)
    , ("M-S-w",             rotateScreens False)
    , ("M-S-e",             rotateScreens True)
    -- Layout
    , ("M-<Space>",         myLayoutPrompt False)
    , ("M-S-<Space>",       myLayoutPrompt True)
    , ("M-m",               maximizeFocusedWindow)
    -- Misc
    , ("M-/",               manPrompt myXPConfig)
    , ("M-;",               mainCommandPrompt)
    -- , ("M-q M-q",           reload)
    , ("M-y",               modifyClipboard)
    , ("M-z",               removeCursor)
    , ("M-<Esc>",           runScript "blur" [])
    , ("M-`",               waitModKeyRelease >> spawn "close-notifier")
    -- Important
    , ("M-S-q M-S-q",       io exitSuccess)
    -- KeyMode
    , ("M-u w",             enterKeyMode myKeyModeTable "Window")
    , ("M-u r",             refreshWindow)
    -- , ("M-u m",             enterKeyMode myKeyModeTable "Mouse")
    , ("<XF86Calculator>",        namedScratchpadAction scratchpads "calc")
    ] ++ fadeKeys ++ snapKeys


fadeKeys = ("M-u a r", withFocused unsetFixedFadeWindow >> refresh)
         : ("M-u a h", fade 0)
         : ("M-u a 0", fade 1)
         : [ ("M-u a " ++ show n, fade (n % 10)) | n <- [1..9] ]
  where
    fade r = withFocused (setFixedFadeWindow r) >> refresh


snapKeys = [ ("M-u s " ++ key, withFocused $ snapWindow dir)
           | dir <- [L, D, U, R]
           | key <- map return "hjkl" ]


myNav2d =
    withNavigation2DConfig def {
      defaultTiledNavigation = centerNavigation
    }


c = controlMask
s = shiftMask
m = myModMask


wc :: Integral i => i -> (i -> X ()) -> X ()
wc = withCount


commonKeys = M.fromList
    [ ((m, xK_j), windows SS.focusDown)
    , ((m, xK_k), windows SS.focusUp) ]


myKeyModeTable :: KeyModeTable
myKeyModeTable = M.map (`M.union` commonKeys) $ M.map M.fromList $ M.fromList
    [ ( "Window" ,
        [ ((0, xK_e),       withFocused $ \w -> shiftNextScreen >> focus w)
        , ((0, xK_h),       wc 10 $ \c -> withFocused $ keysMoveWindow (-c, 0))
        , ((0, xK_l),       wc 10 $ \c -> withFocused $ keysMoveWindow (c, 0))
        , ((0, xK_j),       wc 10 $ \c -> withFocused $ keysMoveWindow (0, c))
        , ((0, xK_k),       wc 10 $ \c -> withFocused $ keysMoveWindow (0, -c))
        , ((s, xK_h),       wc 10 $ \c -> withFocused $ keysResizeWindow (-c, 0) (0, 0))
        , ((s, xK_l),       wc 10 $ \c -> withFocused $ keysResizeWindow (c, 0) (0, 0))
        , ((s, xK_j),       wc 10 $ \c -> withFocused $ keysResizeWindow (0, c) (0, 0))
        , ((s, xK_k),       wc 10 $ \c -> withFocused $ keysResizeWindow (0, -c) (0, 0))
        , ((m, xK_h),       withFocused $ snapWindow L)
        , ((m, xK_j),       withFocused $ snapWindow D)
        , ((m, xK_k),       withFocused $ snapWindow U)
        , ((m, xK_l),       withFocused $ snapWindow R)
        , ((0, xK_x),       wc  0 $ \c -> moveFocusedWindowX c)
        , ((0, xK_y),       wc  0 $ \c -> moveFocusedWindowY c)
        , ((s, xK_x),       wc  0 $ \c -> moveFocusedWindowX' c)
        , ((s, xK_y),       wc  0 $ \c -> moveFocusedWindowY' c)
        , ((0, xK_s),       wc 10 $ \c -> withFocused $ windows . SS.sink)
        , ((s, xK_6),       withFocused $ snapWindow L)
        , ((s, xK_4),       withFocused $ snapWindow R)
        , ((0, xK_g),       withFocused $ snapWindow U)
        , ((s, xK_g),       withFocused $ snapWindow D) ] )
    , ( "Mouse" ,
        [ ((m, xK_j),       windows SS.focusDown >> moveMouseWindowCenter')
        , ((m, xK_k),       windows SS.focusUp >> moveMouseWindowCenter')
        , ((0, xK_e),       nextScreen >> moveMouseWindowCenter')
        , ((0, xK_h),       wc 10 $ \c -> moveMouseRel (-c, 0))
        , ((0, xK_l),       wc 10 $ \c -> moveMouseRel (c, 0))
        , ((0, xK_j),       wc 10 $ \c -> moveMouseRel (0, c))
        , ((0, xK_k),       wc 10 $ \c -> moveMouseRel (0, -c))
        , ((0, xK_x),       wc 0  $ \c -> moveMouseX c)
        , ((0, xK_y),       wc 0  $ \c -> moveMouseY c)
        , ((0, xK_c),       moveMouseWindowCenter')
        , ((s, xK_x),       wc 0  $ \c -> moveMouseX' c)
        , ((s, xK_y),       wc 0  $ \c -> moveMouseY' c)
        , ((0, xK_f),       withFocused $ \w -> do
                               (_, _, cw, ch) <- windowRect w
                               updateWindowPosition w (cw `div` 2, ch `div` 2) )
        , ((0, xK_Return),  mouseClick 1)
        , ((0, xK_Delete),  mouseClick 3) ] ) ]


waitModKeyRelease = waitKeyRelease [xK_Alt_L, xK_Alt_R]

