
{-# LANGUAGE NoMonomorphismRestriction, FlexibleContexts #-}

module Anekos.Config.LogHook (
    mkMyLogHook
) where


-- import Anekos.Config.Action(updatePointer)
import Anekos.Config.Config
import Anekos.Config.ManageHook (isViewer)
import Anekos.Lib.Fade (fadeInactiveLogHook')
import Anekos.Lib.ShowWSName (showWSNameHook)
import Anekos.Lib.Workspace (validWSName)
import Anekos.Lib.LogState (updateState)

import Data.Char (isDigit)
import Data.Functor ((<$>))
import System.IO (hPutStrLn)
import Text.Regex.Posix ((=~))
import XMonad
import XMonad.Hooks.DynamicLog ( dzenPP, dzenColor, wrap, dzenEscape
                               , dynamicLogWithPP , PP(..) )
import XMonad.Hooks.SetWMName (setWMName)
import XMonad.Hooks.ToggleHook (toggleHook)
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.WorkspaceCompare (mkWsSort)


mkMyLogHook = do
    status <- spawnPipe myDzenStatus
    return ( setWMName "LG3D"
             <+> fadeInactiveLogHook' (not <$> isViewer)
             <+> dynamicLogWithPP myDzenPP { ppOutput = hPutStrLn status . (' ':) }
             <+> showWSNameHook myFTConfig
             -- <+> updatePointer
             <+> updateState ) -- updateState は最後に!

myDzenPP  = dzenPP
    { ppCurrent         = ignore $ ppgo "#ffff33" no no no
    , ppVisible         = ignore $ ppgo "#bdbdbd" no no no
    , ppHidden          = ignore $ ppgo "#77782d" no no no
    , ppHiddenNoWindows = const ""
    , ppUrgent          = pp "#ff0000" no "!" "!"
    , ppWsSep           = sp
    , ppSep             = "  "
    , ppLayout          = pp "#aaaaaa" no "^ca(1,xc select layout)-" "-^ca()"
    , ppTitle           = pp "#ffffff" no caWindowSelect (nop ++ nop) . dzenEscape
                             -- shorten 80
    , ppSort            = mkWsSort $ return compare }
  where
    no                = ""
    sp                = " "
    ca btn act        = "^ca(" ++ show btn ++ act ++ ")"
    nop               = "^ca()"
    pp fg bg l r      = dzenColor fg bg . wrap l r
    ppgo fg bg l r s@(_:x:_)
      | isDigit x     = pp fg bg
                          ( "^ca(1,xdotool key alt+shift+h key alt+" ++ [x] ++ ")"
                            ++ "^ca(3,xdotool key alt+shift+l key alt+" ++ [x] ++ ")"
                            ++ l )
                          (r ++ nop ++ nop) s
    ppgo fg bg l r s  = pp fg bg l r s
    ignore f s
      | validWSName s = f s
      | otherwise     = ""
    caWindowSelect    = "^ca(1,xc select window)" ++
                          "^ca(3,xc screen next ; xc select window)"

shortenTitle s = pre ++ post
  where
    (pre, _, post) = s =~ "^\\s*\\[.*\\]\\s*" :: (String, String, String)
