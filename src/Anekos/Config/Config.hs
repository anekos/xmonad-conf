
{-# LANGUAGE NoMonomorphismRestriction, FlexibleContexts #-}

module Anekos.Config.Config (
    module Anekos.Config.Config,
    module Anekos.Config.Env.Common,
    module Anekos.Config.Env.Load
) where


import Anekos.Config.Env.Common
import Anekos.Config.Env.Load

import Anekos.Lib.FlashText (FTConfig(..), flashText)

import Data.Char (toLower)
import Data.Default (Default, def)
import Data.List (isInfixOf)
import Data.Ratio ((%))
import qualified Data.Map as M
import XMonad
import XMonad.Actions.GridSelect (GSConfig(..))
import XMonad.Actions.HitAHint (HAHConfig(..), simpleHints)
import XMonad.Actions.LinkWorkspaces (MessageConfig(..))
import XMonad.Layout.Decoration (Theme(..))
import XMonad.Layout.ImageButtonDecoration (defaultThemeWithImageButtons)
import XMonad.Prompt ( XPConfig(..), XPPosition(..)
                     , defaultXPKeymap, moveHistory,  deleteConsecutive
                     , deleteString, Direction1D(..)
                     , setSuccess, setDone )
import qualified XMonad.StackSet as SS


myWorkspaces :: [WorkspaceId]
myWorkspaces = ["01", "10"]


myTerminal = "force-interval 3s term"
myTerminal' cmd = myTerminal ++ " -e " ++ cmd


myEditor = "xeditor"


myEditor' role = "xeditor -r " ++ role


myModMask = mod4Mask


myHAHConfig = def
    { hahFont        = "xft:Terminus:pixelsize=40:hinting=true:hintstyle=hintslight:antialias=true:rgba=rgb:lcdfilter=lcdlight"
                       -- myFont 40
    , hahHints       = simpleHints "asdfjklgh" }


myFTConfig = def
    { ftFont        = myFont 64
    , ftBgColor     = "#285577"
    , ftColor       = "white"
    , ftBorderColor = "#aaaaaa"
    , ftBorderWidth = 3
    , ftOpacity     = 4%5
    , ftFade        = 1 }


--simpleHints :: [Char] -> [Window] -> X [(Window, String)]
--simpleHints cs ws = return $ zip ws cut
--  where
--    chars = map return cs
--    hints = chars ++ [ a ++ b | a <- hints, b <- chars]
--    cut | length ws <= length cs = hints
--        | otherwise              = drop (length cs) hints


myGSConfig = def
    { gs_cellheight   = 50
    , gs_cellwidth    = 300
    , gs_cellpadding  = 5
    , gs_font         = myFont 22 }


myBorderColor         = "darkred"
myFocusedBorderColor  = "orange"


myColorBG     = "#0c1021"
myColorBG1    = "#2c8fba"
myColorBG2    = "red"
myColorFG     = "#f8f8f8"
myColorBorder = "DarkOrange"


myAutoComplete = Just (1000000 `div` 5)


mySWNConfig = def


myXPConfig = def
    { historyFilter         = deleteConsecutive
    , font                  = myFont 22
    , height                = 30
    , position              = Bottom
    , bgColor               = myColorBG
    , fgColor               = myColorFG
    , fgHLight              = "#f8f8f8"
    , bgHLight              = "steelblue3"
    , borderColor           = myColorBorder
    , promptBorderWidth     = 1
    , completionKey         = (0, xK_Tab)
    , historySize           = 256
    , defaultText           = []
    , autoComplete          = Nothing
    , showCompletionOnTab   = False
    , searchPredicate       = mySearchPredicate
    , promptKeymap          = M.fromList keymap `M.union` defaultXPKeymap }
  where
    mySearchPredicate a b = lc a `isInfixOf` lc b
    lc                    = map toLower
    submit                = setSuccess True >> setDone True
    keymap                =
      [ ((controlMask, xK_k), moveHistory SS.focusDown')
      , ((controlMask, xK_l), moveHistory SS.focusUp')
      , ((controlMask, xK_j), submit)
      , ((controlMask, xK_m), submit)
      , ((controlMask, xK_h), deleteString Prev) ]


myTabTheme = def
    { fontName            = myFont 14
    , activeColor         = myColorBG1
    , inactiveColor       = myColorBG
    , urgentColor         = myColorBG2
    , activeBorderColor   = "grey"
    , inactiveBorderColor = "grey"
    , urgentBorderColor   = "grey"
    , activeTextColor     = myColorFG
    , inactiveTextColor   = myColorFG
    , urgentTextColor     = myColorFG
    , decoWidth           = 200
    , decoHeight          = 20
    , windowTitleAddons   = []
    , windowTitleIcons    = [] }


myButtonsTheme = myTabTheme
    { windowTitleIcons = windowTitleIcons defaultThemeWithImageButtons }


myMessageConfig = MessageConfig
    { messageFunction = message
    , foreground = ""
    , alertedForeground = ""
    , background = "" }
  where
    message _ _ _ = flashText ftc
    ftc = myFTConfig { ftFont = myFont 20
                     , ftFade = 3/2 }
