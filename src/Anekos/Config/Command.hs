
module Anekos.Config.Command (
    mainCommandPrompt,
    runCommand
) where


import Anekos.Config.Action
import Anekos.Config.Config
import Anekos.Config.Layout (setLimitPrompt)
import Anekos.Lib.CommandMaker (mkCommandPrompt)
import Anekos.Lib.Fade (toggleFadeInActive, toggleIgnoreWindowToFade, setFadeInActive, opacityPrompt, opacityPromptFocused)
import Anekos.Lib.Notify (changeNotifyMethod)
import Anekos.Lib.Resume (reload, getXMonadStatus)
import Anekos.Lib.XUtils (withAllOfScreen, focusAnd, withFocusRestore)
import Anekos.Lib.Window (resizeWindowPrompt, enlargeWindow)
import Anekos.Lib.WithBackground (WithBGMessage(..))
import Anekos.Lib.Workspace (renameWorkspacePrompt, moveTo, swapWorkspacePrompt, goShyScreen)

import Control.Monad ((>=>))
import Data.Default (def)
import Data.Function (on)
import Data.List (nubBy, find, isPrefixOf)
import Data.List.Split (splitOn)
import Data.Map (insert)
import XMonad
import XMonad.Actions.Commands (defaultCommands)
import XMonad.Actions.CopyWindow (copy, kill1, killAllOtherCopies)
import XMonad.Actions.DeManage (demanage)
import XMonad.Actions.DynamicWorkspaces (selectWorkspace)
import XMonad.Actions.LinkWorkspaces (toggleLinkWorkspaces)
import XMonad.Actions.TagWindows (addTag, withTaggedGlobal, tagPrompt)
import XMonad.Actions.WithAll (withAll)
import XMonad.Hooks.DebugStack (debugStackString)
import XMonad.Hooks.ManageDocks (ToggleStruts(..))
import XMonad.Hooks.UrgencyHook (focusUrgent)
import XMonad.Prompt (XPConfig(autoComplete, searchPredicate, promptKeymap), setSuccess, setDone, deleteString, Direction1D(..))
import XMonad.Prompt.Input (inputPrompt, (?+))
import XMonad.Prompt.Layout (layoutPrompt)
import XMonad.Prompt.Man (manPrompt)
import XMonad.Prompt.Ssh (sshPrompt)
import XMonad.Util.DebugWindow (debugWindow)
import XMonad.Util.XUtils (deleteWindow)
import qualified XMonad.StackSet as SS


xpc = myXPConfig
wfr = withFocusRestore


myCommands =
    [ ("bg",            do setFadeInActive False
                           withFocused $ sendMessage . SetBackground)
    , ("border",        withFocused toggleBorder)
    , ("cd",            setCurrentDirectoryPrompt xpc)
    , ("close-notifier",spawn "close-notifier")
    , ("copy",          copyPrompt)
    , ("copyall",       windows copyToAll)
    , ("copy-window-cwd",
                        copyFocusedWindowCwd)
    , ("copy-window-id",
                        copyFocusedWindowId)
    , ("copy-window-tty",
                        copyFocusedWindowTty)
    , ("copy-window-pid",
                        copyFocusedWindowPid)
    , ("delete",        tagPrompt xpc $ flip withTaggedGlobal deleteWindow)
    , ("demanage",      withFocused demanage)
    , ("dump-status",   getXMonadStatus >>= io . putStrLn)
    , ("dump-stack",    debugStackString >>= io . putStrLn)
    , ("dump-window",   withFocused (debugWindow >=> (io . putStrLn)))
    , ("echo-clear",    clearFocusedWindowTty)
    , ("enlarge",       withFocused enlargeWindow)
    , ("fade",          toggleFadeInActive $ Just def)
    , ("fix-fade",      opacityPromptFocused xpc $ Just def)
    , ("font-name",     setRxvtFontNamePrompt xpc)
    , ("font-size",     setRxvtFontSizePrompt xpc)
    , ("gyazo",         spawn "gyazo")
    , ("ignore-fade",   withFocused toggleIgnoreWindowToFade)
    , ("key-press",     keypressPrompt xpc)
    , ("kill",          kill)
    , ("kill1",         kill1)
    , ("killother",     killAllOtherCopies)
    , ("layout",        layoutPrompt xpc)
    , ("limit",         setLimitPrompt)
    , ("link",          toggleLinkWorkspaces myMessageConfig)
    , ("notify",        changeNotifyMethod)
    , ("pull",          pullWindowPrompt xpc)
    , ("push",          pushWindowPrompt xpc)
    , ("opacity",       opacityPrompt xpc)
    , ("openselection", openSelectionPrompt)
    , ("refresh",       refresh)
    , ("refresh-window",refreshWindow)
    , ("reload",        reload)
    , ("rename",        renameWorkspacePrompt xpc)
    , ("resize",        resizeWindowPrompt xpc)
    , ("role",          setWindowRolePrompt xpc)
    , ("screen-shot",   screenshot)
    , ("screen-names",  displayScreenName)
    , ("shy",           goShyScreen)
    , ("sink",          withFocused $ windows . SS.sink)
    , ("swap-ws",       swapWorkspacePrompt xpc)
    , ("ssh",           sshPrompt xpc)
    , ("tag",           inputPrompt xpc "Tag" ?+ (withFocused . addTag))
    , ("title",         displayWindowName)
    , ("toggle-struts", sendMessage ToggleStruts)
    , ("unbg",          sendMessage RemoveBackground)
    , ("urgent",        focusUrgent)
    , ("workspace",     selectWorkspace xpc) ]


runCommand :: String -> X ()
runCommand cmd = do
    defs <- defaultCommands
    let found = find ((== cmd) . fst) (myCommands ++ defs)
    whenJust found snd


mainCommandPrompt = do
    defs <- defaultCommands
    mkCommandPrompt xpc' "Command" $ nubBy ((==) `on` fst) $ myCommands ++ defs


xpc' = xpc { searchPredicate = pred, promptKeymap = keyMap, autoComplete = myAutoComplete }
  where
    short = map head . splitOn "-"
    pred x y = searchPredicate xpc x y || x `isPrefixOf` short y
    keyMap = insert (0, xK_space) submit $ promptKeymap xpc
    submit = setSuccess True >> setDone True
