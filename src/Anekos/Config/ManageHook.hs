
module Anekos.Config.ManageHook where


import Anekos.Config.Monitor (myMonitor)
import Anekos.Lib.Query ((=^?), (=$?), wmIconName, wmRole, shellName, title, wmName, layoutName, hasProp)
import Anekos.Lib.Window (enlargeWindow, updateWindowPosition, updateWindowPosition')
import Anekos.Lib.Workspace ( shiftWorkspace, shiftScreen, addWorkspace, viewNthScreen
                            , updateWorkspaceId, modifyWorkspaceName)

import Control.Monad (filterM)
import Data.Default (Default, def)
import Data.Functor ((<$>))
import Data.Monoid (mempty)
import XMonad hiding (doShift)
import qualified XMonad.StackSet as SS
import XMonad.Actions.PhysicalScreens (PhysicalScreen)
import XMonad.Layout.LayoutCombinators (JumpToLayout(..))
import XMonad.Layout.Monitor (manageMonitor)
import XMonad.Hooks.ManageDocks (manageDocks)
import XMonad.Hooks.ManageHelpers (composeOne, (-?>), doCenterFloat, isDialog)
import XMonad.Hooks.ToggleHook (toggleHook)
import XMonad.Util.XUtils (hideWindow)


myManageHook :: ManageHook -> ManageHook
myManageHook add =
    manageDocks <+>
    manageMonitor myMonitor <+>
    toggleHook "shift-another-screen" doShiftAnotherScreen <+>
    toggleHook "viewer"               (isViewer --> doShiftAnotherScreen) <+>
    composeAll
      [ layoutName =? "Float"             --> doSink
      , className =? "Ario"               --> doShiftScreen 0
      , className =? "Gxmessage"          --> doCenterFloat
      , className =? "Lazarus"            --> doFloat
      , className =? "Lcl"                --> doCenterFloat
      , className =? "Savebox"            --> doFloat
      , className =? "XOsview"            --> doFloat
      , className =? "Xmessage"           --> doCenterFloat
      , className =? "Xsensors"           --> doFloat
      , className =? "Conky"              --> doCenterFloat
      , className =? "Screenkey"          --> doIgnore
      , className =? "XEyes"              --> doTopRightFloat
      , className =^? "sdl"               --> doCenterFloat
      , className =? "Xephyr"             --> doSink
      , className =? "Gtk-recordMyDesktop"--> doFloat
      , className =? "Gnuplot"            --> doShiftAnotherScreen
      , wmRole    =? "STOCKING"           --> doHide
      , title     =? "Gopher"             --> doIgnore
      , className =? "VirtualBox"         --> doFloat
      , wmIconName=? "chrysoberyl-shell"  --> doBottomRightFloat
      , className =? "zenity"             --> doCenterFloat
      , className =? "Yad"                --> doCenterFloat
      , shellName "Menu Shell"            --> doCenterShell
      , wmRole    =? "fix-display-dots"   --> doCenterFloat
      , wmRole    =? "Vimenu"             --> doCenterFloat
      , className =? "Xfce4-notifyd"      --> doIgnore
      ]
      <+> manageSteamGame
      <+> manageDevApps
      <+> manageDia
      <+> manageIMs
      <+> manageDialogs
      <+> manageQEMU
      <+> manageDesight
      <+> manageV2c
      <+> add
      <+> manageHook def


doMove :: (Rational, Rational) -> ManageHook
doMove xy = ask >>= liftX . flip updateWindowPosition' xy >>= idHook


isViewer :: Query Bool
isViewer = or <$> mapM (className =?) viewerClasses


onLayout :: String -> Query Bool
onLayout l = ask >>= \w -> liftX $ do
    wss <- gets (SS.workspaces . windowset)
    wss' <- filterM (return . f w) wss
    return $ not $ null wss'
  where
    f w ws = (l == description (SS.layout ws)) && elem w (SS.integrate' $ SS.stack ws)


viewerClasses :: [String]
viewerClasses = ["feh", "Mirage", "Mcomix", "Smplayer", "Vlc", "mplayer2", "Gnuplot", "Sxiv", "mpv", "eitaro"]


manageDevApps :: ManageHook
manageDevApps = composeAll
    [ className =? "ruby" --> doCenterFloat
    -- , className =? "Apvlv" --> doCenterFloat
    , className =? "Test.rb" --> doCenterFloat ]


-- IM
manageIMs :: ManageHook
manageIMs = composeAll [ c --> doCenterFloat | c <- conds ]
  where
    conds = [ className =? "Pidgin" <&&> wmRole =? "conversation" ]


-- manageDia -> dia のツールボックスなどを適当に配置
manageDia :: ManageHook
manageDia =
    className =? "Dia" --> (doNamedShift' "Dia" <+> doJumpToLayout "Dia")
    <+> composeOne
    [ wmRole =? "properties_window"           -?> doSink
    , wmRole =? "diagram_window"              -?> doSink
    , wmRole =? "toolbox_window"              -?> doSink
    , return True                             -?> doFloat ]


-- manageSkype -> Skype
manageSkype :: ManageHook
manageSkype = composeOne
    [ crole "CallWindow"                  -?> doFloat
    , crole "ConversationsWindow"         -?> doSink ]
  where
    crole name = className =? "Skype" <&&> wmRole =? name


-- manageQEMU
manageQEMU :: ManageHook
manageQEMU =
    className =? "qemu-system-x86_64" --> (doNamedShift' "qemu" <+> doJumpToLayout "Full")


manageDialogs :: ManageHook
manageDialogs = isDialog --> doCenterFloat


manageDesight :: ManageHook
manageDesight = composeAll
    [ className =? "XTerm" <&&> wmIconName =? "sbcl" --> doBan ]


manageV2c :: ManageHook
manageV2c = composeAll
    [ className =? "V2C" <&&> title =^? "win" --> doIgnore ]


manageSteamGame :: ManageHook
manageSteamGame = composeAll
    [ hasProp "STEAM_GAME" --> doSink ]


-- Float とかの組み合わせ

doSink :: ManageHook
doSink = ask >>= doF . SS.sink


doShift, doShiftFloat :: Int -> ManageHook
doShiftFloat n = doFloat <+> doF (shiftWorkspace n)
doShift n      = doF (shiftWorkspace n)


doShiftScreen :: ScreenId -> ManageHook
doShiftScreen sid  = doF $ shiftScreen sid


doShiftAnotherScreen :: ManageHook
doShiftAnotherScreen = liftX otherScreen >>= (doF . shiftScreen)
  where
    otherScreen = do
      sid <- gets (SS.screen . SS.current . windowset)
      return $ if sid == 0 then 1 else 0


doNamedShift :: WorkspaceId -> ManageHook
doNamedShift n = liftX (addWorkspace n) >>= (doF . SS.shift)


doNamedShift' :: WorkspaceId -> ManageHook
doNamedShift' n = liftX addNewWS >>= (doF . SS.shift)
  where
    addNewWS = withWindowSet $ \ws -> do
      let wksp = SS.workspace $ SS.current ws
      case SS.stack wksp of
        (Just _)  -> addWorkspace n
        _         -> do let n' = modifyWorkspaceName (SS.tag wksp) n
                        windows $ updateWorkspaceId n'
                        return n'

doBan :: ManageHook
doBan = doF $ SS.shift "NSP"


doHide :: ManageHook
doHide = ask >>= \w -> liftX (hideWindow w) >> doF (SS.delete w)


doJumpToLayout :: String -> ManageHook
doJumpToLayout l = liftX (sendMessage $ JumpToLayout l) >>= idHook


doViewScreen :: PhysicalScreen -> ManageHook
doViewScreen n = liftX (viewNthScreen n) >>= idHook


doFlopDown :: ManageHook
doFlopDown = doF (SS.focusUp . SS.swapDown)


doTopRightFloat :: ManageHook
doTopRightFloat = ask
                  >>= \w -> doF . SS.float w . position . snd
                  =<< liftX (floatLocation w)
  where position (SS.RationalRect _ _ w h) = SS.RationalRect (1-w) 0.03 w h


doTopLeftFloat :: ManageHook
doTopLeftFloat = ask
                 >>= \w -> doF . SS.float w . position . snd
                 =<< liftX (floatLocation w)
  where position (SS.RationalRect _ _ w h) = SS.RationalRect 0 0.03 w h


doBottomRightFloat :: ManageHook
doBottomRightFloat = ask
                     >>= \w -> doF . SS.float w . position . snd
                     =<< liftX (floatLocation w)
  where position (SS.RationalRect _ _ w h) = SS.RationalRect (1-w) (1-h) w h


doBottomLeftFloat :: ManageHook
doBottomLeftFloat = ask
                    >>= \w -> doF . SS.float w . position . snd
                    =<< liftX (floatLocation w)
  where position (SS.RationalRect _ _ w h) = SS.RationalRect 0 (1-h) w h


doFocus :: ManageHook
doFocus = ask >>= liftX . windows . SS.focusWindow >>= idHook


doEnlarge :: ManageHook
doEnlarge = ask >>= liftX . enlargeWindow >>= idHook


doCenterShell :: ManageHook
doCenterShell  = ask
                 >>= \w -> doF . SS.float w . position . snd
                 =<< liftX (floatLocation w)
  where position = const $ SS.RationalRect (1/4) (1/6) (2/4) (4/6)


-- This action breaks the window.
-- doBg :: ManageHook
-- doBg = ask >>= liftX . io . print >> ask >>= liftX . sendMessage . SetBackground >>= mempty
