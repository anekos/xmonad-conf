
if [ "$XMONAD_LAUNCHER" = 1 ] || [ "$XMONAD_SHELL" = 1 ] # {{{
then

  mkdir -p /tmp/xmosh
  mkdir -p /tmp/xmonad-launcher
  cd /tmp/xmosh

  alias ff=~/script/firefox/open-from-places-sqlite

  # 特定のフックだけ除外
  preexec_functions=( ${preexec_functions:#_anekos_auto_shell_title_preexec} )

PROMPT="%F{red}┏%F{yellow}%B\${_anekos_prompt_lrc}[XMonad:%~]%b %F{green}%T%f
%F{red}┗%F{blue}▶%f "


  # Esc からはじまるキーバインドを削除する
  # これにより、すぐに閉じることができる。
  # ただし、`Bracketed paste mode` のバインディングも消えてしまうので、
  # コピペ時に `[200` のような文字列も挿入されてしまう
  #
  # bindkey -rp "^["  # `-r` remove, `-p` prefix

  # 空コマンド(?) 実行時に rehash する。
  function _anekos_xmonad_launcher_on_return () {
    zle accept-line
    [ -z "$BUFFER" ] || return 0
    echo -n "\e[1;33mRehashed\e[0m"
    _anekos_xmonad_launcher_do_close=0
    rehash
  }
  zle -N _anekos_xmonad_launcher_on_return
  bindkey "\C-m" _anekos_xmonad_launcher_on_return

  alias vim="vim -X"
  if [[ "$EDITOR" =~ "vim.*" ]]
  then
    export EDITOR="$EDITOR -X"
  fi

fi # }}}

if [ "$XMONAD_LAUNCHER" = 1 ] # {{{
then
  function tmux {
    /bin/tmux -S /tmp/xmonad-launcher/tmux-launcher-session -f ~/.tmux.xmonad-launcher.conf
  }

  HISTFILE=~/.xmonad_launcher_history

  echo -ne "\033]0;XMonad Launcher\007"

  function _anekos_xmonad_launcher_close {
    xc scratchpad zsh
  }

  function _anekos_xmonad_launcher_run {
    local name="$1"
    # local tmpout=` mktemp "/tmp/xmonad-launcher/${name}-XXXXXXX" `
    # chmod go-rwx "$tmpout"

    # 特定のアプリで環境変数を上書きする
    local GDK_SCALE="${GDK_SCALE:-1}"
    case "$name" in
      gvim)
        GDK_SCALE=1
        ;;
    esac

    # GDK_SCALE="$GDK_SCALE" LC_ALL=ja_JP.UTF-8 setsid "$@" &> /dev/null
    GDK_SCALE="$GDK_SCALE" setsid "$@" &> /dev/null
    # GDK_SCALE="$GDK_SCALE" LC_ALL=ja_JP.UTF-8 setsid "$@" &> "$tmpout" &

    clear
  }

  zle -N _anekos_xmonad_launcher_close

  bindkey '^[' _anekos_xmonad_launcher_close

  function _anekos_xmonad_launcher_preexec () {
    local -a cmd
    cmd=(${(z)2})
    local full=$3
    local name=$cmd[1]

    _anekos_xmonad_launcher_do_close=0

    case "$1" in
      ";"* | " "*)
        return 0
      ;;
      *";")
        if [ -x "`which $name`" ]
        then
          _anekos_xmonad_launcher_do_close=1
          eval "
          function $name {
            unfunction $name
            (urxvt -pt root -title '$name' -e '$full') &  # &!
          }
          "
        fi
        return 0
      ;;
    esac

    case $name in
      "autojump" | "j" | "cd" | "ls" | "c" | ";"* | " "* | tmux)
      ;;
      xranger)
        _anekos_xmonad_launcher_do_close=1
      ;;
      *)
        # エイリアスの時もある…
        if has-command "$name"
        then
          _anekos_xmonad_launcher_do_close=1
          eval "
          function $name {
            unfunction $name
            _anekos_xmonad_launcher_run $full
          }
          "
        fi
      ;;
    esac

  }

  function _anekos_xmonad_launcher_precmd {
    local r=$?

    echo -ne "\033]0;XMonad Launcher\007"

    [ "$r" != 0 ] && return
    [ "$_anekos_xmonad_launcher_do_close" = 1 ] || return 0
    _anekos_xmonad_launcher_close
  }

  preexec_functions=($preexec_functions _anekos_xmonad_launcher_preexec)
  precmd_functions=($precmd_functions _anekos_xmonad_launcher_precmd)

  XMONAD_LAUNCHER=0

  alias ranger=xranger
  has-command afk && alias slock=afk

fi # }}}

if [ "$XMONAD_SHELL" = 1 ] # {{{
then
  function tmux {
    /bin/tmux -S /tmp/xmonad-launcher/tmux-shell-session -f ~/.tmux.xmonad-shell.conf
  }

  echo -ne "\033]0;XMonad Shell\007"

  function _anekos_xmonad_shell_close {
    xc scratchpad shell
  }

  #zle -N _anekos_xmonad_shell_on_return
  zle -N _anekos_xmonad_shell_close

  #bindkey '^M' _anekos_xmonad_shell_on_return
  bindkey '^[' _anekos_xmonad_shell_close

  function _anekos_xmonad_shell_precmd {
    echo -ne "\033]0;XMonad Shell\007"
  }

  precmd_functions=($precmd_functions _anekos_xmonad_shell_precmd)

  XMONAD_SHELL=0
fi # }}}

