
.PHONY: build main link

main: build link

build:
	rm -rf .stack-work/install
	stack build

link: xmonad xmonad-x86_64-linux xc

xmonad: build
	- unlink xmonad
	ln -s $(shell find .stack-work/install/ -name xmonad -type f -perm 755) xmonad

xmonad-x86_64-linux: build
	- unlink xmonad-x86_64-linux
	ln -s $(shell find .stack-work/install/ -name xmonad -type f -perm 755) xmonad-x86_64-linux

xc:
	- unlink xc
	ln -s $(shell find .stack-work/install/ -name xc -type f -perm 755) xc

watch:
	axe $(shell find src -type f -name '*.hs') - stack build

stack.yaml: xmonad-anekos.cabal
	stack init --force --resolver=lts-17.9
